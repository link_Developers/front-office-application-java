/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.resturent.main;

import com.example.resturent.view.controller.HomePageController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * @author Janindu C Perera
 */
public class HomePage extends Application {


    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);

    }

    @Override
    public void start(Stage stage) throws Exception {
//        Parent root = FXMLLoader.load(getClass().getResource("/com/example/resturent/view/HomePage.fxml"));
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/com/example/resturent/view/HomePage.fxml"));
        Parent root = fxmlLoader.load();
        HomePageController controller = fxmlLoader.getController();
        controller.start(stage);
        stage.initStyle(StageStyle.UNDECORATED);
        Scene scene = new Scene(root);
//        stage.setAlwaysOnTop(true);
        stage.setTitle("Hotel Front Office");
        stage.setScene(scene);
        stage.show();
    }
    
}
