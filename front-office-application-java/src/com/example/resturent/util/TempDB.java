package com.example.resturent.util;

import com.example.resturent.dto.BookingReturnDTO;

public class TempDB {
    private static BookingReturnDTO bookingReturnDTO;

    public static BookingReturnDTO getBookingReturnDTO() {
        return bookingReturnDTO;
    }

    public static void setBookingReturnDTO(BookingReturnDTO bookingReturnDTO) {
        TempDB.bookingReturnDTO = bookingReturnDTO;
    }
}
