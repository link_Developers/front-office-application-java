package com.example.resturent.util;

import com.example.resturent.api.MultipartUploadUtility;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class UploadTask {
    private String uploadURL;
    private File uploadFile;

    public UploadTask(String uploadURL, File uploadFile) throws Exception {
        this.uploadURL = uploadURL;
        this.uploadFile = uploadFile;

    }

    /**
     * Executed in background thread
     */

    public Void doInBackground() throws Exception {
        try {
            MultipartUploadUtility util = new MultipartUploadUtility(uploadURL,
                    "UTF-8");
            util.addFilePart("file", uploadFile);

            FileInputStream inputStream = new FileInputStream(uploadFile);
            byte[] buffer = new byte[4096];
            int bytesRead = -1;
            long totalBytesRead = 0;
            int percentCompleted = 0;
            long fileSize = uploadFile.length();

            while ((bytesRead = inputStream.read(buffer)) != -1) {
                util.writeFileBytes(buffer, 0, bytesRead);
                totalBytesRead += bytesRead;
                percentCompleted = (int) (totalBytesRead * 100 / fileSize);

            }

            inputStream.close();
            util.finish();
        } catch (IOException ex) {
        }

        return null;
    }

    /**
     * Executed in Swing's event dispatching thread
     */

}
