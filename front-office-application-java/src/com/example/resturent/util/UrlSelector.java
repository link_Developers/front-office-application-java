package com.example.resturent.util;

public class UrlSelector {
    private static String BASE_URL = "http://192.168.1.2:8888/";

    public static String getBaseUrl() {
        return BASE_URL;
    }

    public static void setBaseUrl(String url) {
        BASE_URL = url;
    }
}
