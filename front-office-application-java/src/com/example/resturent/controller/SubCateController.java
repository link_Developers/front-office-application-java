package com.example.resturent.controller;

import com.example.resturent.dto.SubCateDTO;
import com.example.resturent.repository.SubCateRepository;

import java.util.ArrayList;

public class SubCateController implements SuperController<SubCateDTO> {
    private SubCateRepository subCateRepository;

    public SubCateController() {
        subCateRepository = new SubCateRepository();
    }

    @Override
    public boolean add(SubCateDTO subCateDTO) throws Exception {
        return subCateRepository.add(subCateDTO);
    }

    @Override
    public boolean update(SubCateDTO subCateDTO) throws Exception {
        return subCateRepository.update(subCateDTO);
    }

    @Override
    public SubCateDTO find(String id) throws Exception {
        return subCateRepository.find(id);
    }

    @Override
    public ArrayList<SubCateDTO> getAll() throws Exception {
        return subCateRepository.getAll();
    }

    @Override
    public boolean remove(String id) throws Exception {
        return subCateRepository.remove(id);
    }

    public ArrayList<SubCateDTO> getSubCategoryByMajorCategoryId(String id) {
        return subCateRepository.findSubCateByMajorCateId(id);
    }
}
