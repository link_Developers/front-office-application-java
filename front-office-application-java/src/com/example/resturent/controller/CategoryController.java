package com.example.resturent.controller;

import com.example.resturent.dto.MajorCateDTO;
import com.example.resturent.repository.MajorCategoryRepository;

import java.util.ArrayList;

public class CategoryController implements SuperController<MajorCateDTO> {
    private MajorCategoryRepository majorCategoryRepository;

    public CategoryController() {
        majorCategoryRepository = new MajorCategoryRepository();
    }

    @Override
    public boolean add(MajorCateDTO majorCateDTO) throws Exception {

        return majorCategoryRepository.add(majorCateDTO);
    }

    @Override
    public boolean update(MajorCateDTO majorCateDTO) throws Exception {
        return majorCategoryRepository.update(majorCateDTO);
    }

    @Override
    public MajorCateDTO find(String id) throws Exception {
        return majorCategoryRepository.find(id);
    }

    @Override
    public ArrayList<MajorCateDTO> getAll() throws Exception {
        return majorCategoryRepository.getAll();
    }

    @Override
    public boolean remove(String id) throws Exception {
        return majorCategoryRepository.remove(id);
    }

    public ArrayList<String> getMajorCateNames() throws Exception {
        ArrayList<String> cateNames = new ArrayList<>();
        ArrayList<MajorCateDTO> all = majorCategoryRepository.getAll();
        for (MajorCateDTO m :
                all) {
            cateNames.add(m.getCate_name())
            ;
        }
        return cateNames;
    }
}
