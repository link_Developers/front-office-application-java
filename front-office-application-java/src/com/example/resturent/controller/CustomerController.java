package com.example.resturent.controller;

import com.example.resturent.dto.CustomerDTO;
import com.example.resturent.repository.CustomerRepository;

import java.util.ArrayList;

public class CustomerController implements SuperController<CustomerDTO> {
    private CustomerRepository customerRepository;

    public CustomerController() {
        customerRepository = new CustomerRepository();
    }

    @Override
    public boolean add(CustomerDTO customerDTO) throws Exception {
        return customerRepository.add(customerDTO);

    }

    @Override
    public boolean update(CustomerDTO customerDTO) throws Exception {


        return customerRepository.update(customerDTO);
    }

    @Override
    public CustomerDTO find(String id) throws Exception {
        return customerRepository.find(id);

    }

    @Override
    public ArrayList<CustomerDTO> getAll() throws Exception {
        return customerRepository.getAll();

    }

    @Override
    public boolean remove(String id) throws Exception {
        return customerRepository.remove(id);

    }


}
