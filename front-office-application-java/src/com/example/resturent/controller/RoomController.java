package com.example.resturent.controller;

import com.example.resturent.dto.RoomDTO;
import com.example.resturent.repository.RoomRepository;

import java.util.ArrayList;

public class RoomController implements SuperController<RoomDTO> {
    private RoomRepository roomRepository;

    public RoomController() {
        roomRepository = new RoomRepository();
    }

    @Override
    public boolean add(RoomDTO roomDTO) throws Exception {
        return roomRepository.add(roomDTO);
    }

    @Override
    public boolean update(RoomDTO roomDTO) throws Exception {
        return roomRepository.update(roomDTO);
    }

    @Override
    public RoomDTO find(String id) throws Exception {
        return roomRepository.find(id);
    }

    @Override
    public ArrayList<RoomDTO> getAll() throws Exception {
        return roomRepository.getAll();
    }

    @Override
    public boolean remove(String id) throws Exception {
        return roomRepository.remove(id);
    }

    public ArrayList<RoomDTO> getAllAvailable() throws Exception {
        return roomRepository.getAllAvailable();
    }

    public ArrayList<RoomDTO> getAllOccupiedRooms() throws Exception {
        return roomRepository.getAllOccupiedRooms();
    }
}
