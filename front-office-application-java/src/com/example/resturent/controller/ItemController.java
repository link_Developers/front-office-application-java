package com.example.resturent.controller;

import com.example.resturent.dto.ItemDTO;
import com.example.resturent.repository.ItemRepository;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

import java.io.File;
import java.util.ArrayList;

public class ItemController implements SuperController<ItemDTO> {
    private ItemRepository itemRepository;

    public ItemController() {
        itemRepository = new ItemRepository();
    }

    @Override
    public boolean add(ItemDTO itemDTO) throws Exception {
        return false;
    }

    @Override
    public boolean update(ItemDTO itemDTO) throws Exception {
        return false;
    }

    @Override
    public ItemDTO find(String id) throws Exception {
        return null;
    }

    @Override
    public ArrayList<ItemDTO> getAll() throws Exception {
        return null;
    }

    @Override
    public boolean remove(String id) throws Exception {
        return false;
    }

    public String uploadImage(File multipartBody) throws Exception {
        return itemRepository.uploadImage(multipartBody);
    }

    public void uploadImage(File file, EventHandler<MouseEvent> mouseEventEventHandler) {
        this.itemRepository.setHandler(mouseEventEventHandler);
    }
}
