package com.example.resturent.controller;

import com.example.resturent.dto.BookingDTO;
import com.example.resturent.dto.BookingReturnDTO;
import com.example.resturent.repository.BookingRepository;

import java.util.ArrayList;

public class BookingManageController implements SuperController<BookingDTO> {
    private BookingRepository bookingRepository;

    public BookingManageController() {

        this.bookingRepository = new BookingRepository();
    }

    @Override
    public boolean add(BookingDTO bookingDTO) throws Exception {
        return this.bookingRepository.add(bookingDTO);
    }

    @Override
    public boolean update(BookingDTO bookingDTO) throws Exception {
        return false;
    }

    @Override
    public BookingDTO find(String id) throws Exception {
        return null;
    }

    @Override
    public ArrayList<BookingDTO> getAll() throws Exception {
        return null;
    }

    @Override
    public boolean remove(String id) throws Exception {
        return false;
    }

    public BookingReturnDTO getBookingByCustomerId(String id) {
        return this.bookingRepository.getDetailsByCustomerId(id);
    }

    public BookingReturnDTO getBookingByRoomId(String id) {
        return this.bookingRepository.getDetailsByRoomId(id);
    }
}
