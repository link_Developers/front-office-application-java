package com.example.resturent.repository;

import com.example.resturent.dto.BookingDTO;
import com.example.resturent.dto.BookingReturnDTO;
import com.example.resturent.dto.RoomDTO;
import com.example.resturent.util.Print;
import com.example.resturent.util.UrlSelector;
import com.google.gson.Gson;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class BookingRepository implements SuperRepository<BookingDTO> {
    private static String CustomUrl = UrlSelector.getBaseUrl() + "booking";
    private static int responseCode;

    @Override
    public boolean add(BookingDTO bookingDTO) throws Exception {
        System.out.println("start Count- " + bookingDTO.getRooms().size());
        StringBuffer output = new StringBuffer(110);
        output.append("{\n" +
                "\"userId\": \"" + bookingDTO.getUserId() + "\", " +
                "\"bookingMethod\": \" " + bookingDTO.getBookingMethod() + "\"," +
                "\"rooms\": " + "[ ");
        int x = 0;
        for (RoomDTO r : bookingDTO.getRooms()) {
            x++;
            output.append(
                    "{\"" +
                            "roomNumber\":\"" + r.getRoomNumber() + "\"," +
                            "\"roomType\":\"" + r.getRoomType() + "\"," +
                            "\"password\":\"" + r.getPassword() + "\"," +
                            "\"bedCount\":" + r.getBedCount() + "," +
                            "\"price\":" + r.getPrice() + "," +
                            "\"guest\":" + r.getUserId() + "," +
                            "\"userId\":" + r.getUserId() + "," +
                            "\"status\":" + r.getStatus() + "," +
                            "\"createdAt\":\"" + r.getCreatedAt() + "\"" +
                            "}"
            );
            if (bookingDTO.getRooms().size() != x) {
                output.append(",");
            }

        }
        x = 0;
        output.append("]}");
        String json = output.toString();
        String query_url = CustomUrl + "/add";
        try {
            URL url = new URL(query_url);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(5000);
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod("POST");
            OutputStream os = conn.getOutputStream();
            os.write(json.getBytes("UTF-8"));
            os.close();
            Print.info("Send :" + url + " Data:" + json);
            InputStream in = new BufferedInputStream(conn.getInputStream());
            String result = IOUtils.toString(in, "UTF-8");
            if (result == "true") {
                return true;
            }
            System.out.println("result after Reading JSON Response");
            JSONObject myResponse = new JSONObject(result);
            System.out.println("Json Object: " + myResponse);
            in.close();
            conn.disconnect();
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
        return false;

    }

    @Override
    public boolean update(BookingDTO bookingDTO) throws Exception {
        return false;
    }

    @Override
    public BookingDTO find(String id) throws Exception {
        return null;
    }

    @Override
    public ArrayList<BookingDTO> getAll() throws Exception {
        return null;
    }

    @Override
    public boolean remove(String id) throws Exception {
        return false;
    }

    public BookingReturnDTO getDetailsByRoomId(String roomNo) {
        int code = 0;
        String query_url = CustomUrl + "/getByRoomId/" + roomNo;
        try {
            URL obj = null;

            obj = new URL(query_url);

            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            responseCode = con.getResponseCode();
            code = responseCode;
            System.out.println("\nSending 'GET' request to URL : " + query_url);
            System.out.println("Response Code : " + responseCode);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            System.out.println(response);

            Gson gson = new Gson();
            BookingReturnDTO customerDTO = gson.fromJson(response.toString(), BookingReturnDTO.class);

            in.close();
            return customerDTO;
        } catch (IOException e) {
            if (responseCode == 500) {
//                throw new CustomerNotFound();
            } else {
//                throw new CheckDetailsException();
            }
        }

        return null;

    }

    public BookingReturnDTO getDetailsByCustomerId(String custId) {
        int code = 0;
        String query_url = CustomUrl + "/getByCustomerId/" + custId;
        try {
            URL obj = null;

            obj = new URL(query_url);

            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            responseCode = con.getResponseCode();
            code = responseCode;
            System.out.println("\nSending 'GET' request to URL : " + query_url);
            System.out.println("Response Code : " + responseCode);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            System.out.println(response);

            Gson gson = new Gson();
            BookingReturnDTO customerDTO = gson.fromJson(response.toString(), BookingReturnDTO.class);

            in.close();
            return customerDTO;
        } catch (IOException e) {
            if (responseCode == 500) {
//                throw new CustomerNotFound();
            } else {
//                throw new CheckDetailsException();
            }
        }

        return null;
    }
}
