package com.example.resturent.repository;

import com.example.resturent.dto.ItemDTO;
import com.example.resturent.util.UploadTask;
import com.example.resturent.util.UrlSelector;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class ItemRepository implements SuperRepository<ItemDTO> {
    private EventHandler<MouseEvent> event;

    @Override
    public boolean add(ItemDTO itemDTO) throws Exception {
        return false;
    }

    @Override
    public boolean update(ItemDTO itemDTO) throws Exception {
        return false;
    }

    @Override
    public ItemDTO find(String id) throws Exception {
        return null;
    }

    @Override
    public ArrayList<ItemDTO> getAll() throws Exception {
        return null;
    }

    @Override
    public boolean remove(String id) throws Exception {
        return false;
    }

    public String uploadImage(File fileTo) throws IOException {
//        URL url = new URL(query_url);
        try {

            UploadTask uploadTask = new UploadTask(UrlSelector.getBaseUrl() + "/uploadFile", fileTo);
            uploadTask.doInBackground();
//            File file = new File("C:/Hotel/30127498_998687236949585_3635787851148413309_n.jpg");
//            System.out.println(fileTo.getPath());
//            URL url = new URL(UrlSelector.getBaseUrl() + "uploadFile");
//            HttpURLConnection urlconnection = (HttpURLConnection) url.openConnection();
//            urlconnection.setDoOutput(true);
//            urlconnection.setDoInput(true);
//            urlconnection.setRequestMethod("POST");
//            urlconnection.setRequestProperty("Content-type", "multipart/form-data");
//            urlconnection.connect();
//            MultipartBody.Part[] parts = new MultipartBody.Part[5];
//            parts[0]=new FilePart("file",
//            BufferedOutputStream bos = new BufferedOutputStream(urlconnection.getOutputStream());
//            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
//            int i;
//            // read byte by byte until end of stream
//            while ((i = bis.read()) > 0) {
//                bos.write(i);
//            }
//            bis.close();
//            bos.close();
//            System.out.println(((HttpURLConnection) urlconnection).getResponseMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public void setHandler(EventHandler<MouseEvent> mouseEventEventHandler) {
        this.event = mouseEventEventHandler;
    }
}
