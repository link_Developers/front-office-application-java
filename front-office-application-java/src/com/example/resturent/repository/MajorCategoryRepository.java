package com.example.resturent.repository;

import com.example.resturent.dto.MajorCateDTO;
import com.example.resturent.exception.CheckDetailsException;
import com.example.resturent.exception.CustomerNotFound;
import com.example.resturent.util.Print;
import com.example.resturent.util.UrlSelector;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import java.io.*;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;

public class MajorCategoryRepository implements SuperRepository<MajorCateDTO> {
    private static String CustomerUrl = UrlSelector.getBaseUrl() + "category/major";
    private static int responseCode;

    @Override
    public boolean add(MajorCateDTO majorCateDTO) throws Exception {
        String query_url = CustomerUrl + "/add";

        String json = "{\n" +
                "    \"cate_id\": 32,\n" +
                "    \"cate_name\": \"" + majorCateDTO.getCate_name() + "\",\n" +
                "    \"created_at\": \"2020-03-28\",\n" +
                "    \"status\": 1\n" +
                "}";

        try {
            URL url = new URL(query_url);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(5000);
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod("POST");
            OutputStream os = conn.getOutputStream();
            os.write(json.getBytes("UTF-8"));
            os.close();
            Print.info("Send :" + url + " Data:" + json);
            InputStream in = new BufferedInputStream(conn.getInputStream());
            String result = IOUtils.toString(in, "UTF-8");
            if (result == "true") {
                return true;
            }
            System.out.println("result after Reading JSON Response");
            JSONObject myResponse = new JSONObject(result);
            System.out.println("Json Object: " + myResponse);
            in.close();
            conn.disconnect();
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
        return false;
    }

    @Override
    public boolean update(MajorCateDTO majorCateDTO) throws Exception {
        String query_url = CustomerUrl + "/update";

        String json = "{\n" +
                "    \"cate_id\": " + majorCateDTO.getCate_id() + "\",\n" +
                "    \"cate_name\": \"" + majorCateDTO.getCate_name() + "\",\n" +
                "    \"created_at\": \"2020-03-28\",\n" +
                "    \"status\": 1\n" +
                "}";

        try {
            URL url = new URL(query_url);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(5000);
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod("POST");
            OutputStream os = conn.getOutputStream();
            os.write(json.getBytes("UTF-8"));
            os.close();
            Print.info("Send :" + url + " Data:" + json);
            InputStream in = new BufferedInputStream(conn.getInputStream());
            String result = IOUtils.toString(in, "UTF-8");
            if (result == "true") {
                return true;
            }
            System.out.println("result after Reading JSON Response");
            JSONObject myResponse = new JSONObject(result);
            System.out.println("Json Object: " + myResponse);
            in.close();
            conn.disconnect();
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
        return false;
    }

    @Override
    public MajorCateDTO find(String id) throws Exception {
        int code = 0;
        String query_url = CustomerUrl + "/find/" + id;
        try {
            URL obj = null;

            obj = new URL(query_url);

            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            responseCode = con.getResponseCode();
            code = responseCode;
            System.out.println("\nSending 'GET' request to URL : " + query_url);
            System.out.println("Response Code : " + responseCode);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            System.out.println(response);

            Gson gson = new Gson();
            MajorCateDTO majorCateDTO = gson.fromJson(response.toString(), MajorCateDTO.class);

            in.close();
            return majorCateDTO;
        } catch (IOException e) {
            if (responseCode == 500) {
                throw new CustomerNotFound();
            } else {
                throw new CheckDetailsException();
            }
        }
    }

    @Override
    public ArrayList<MajorCateDTO> getAll() throws Exception {
        ArrayList<MajorCateDTO> sendCustomerDTOS = new ArrayList<>();
        String query_url = CustomerUrl + "/";
        try {
            URL obj = null;
            obj = new URL(query_url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + query_url);
            System.out.println("Response Code : " + responseCode);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            System.out.println(response);
            Gson gson = new Gson();
            Type collectionType = new TypeToken<Collection<MajorCateDTO>>() {
            }.getType();
            Collection<MajorCateDTO> enums = gson.fromJson(response.toString(), collectionType);
            for (MajorCateDTO c :
                    enums) {
                sendCustomerDTOS.add(c);
            }
            in.close();
            return sendCustomerDTOS;
        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }

    @Override
    public boolean remove(String id) throws Exception {
        String query_url = CustomerUrl + "/remove/" + id;
        try {
            URL obj = null;

            obj = new URL(query_url);

            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + query_url);
            System.out.println("Response Code : " + responseCode);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            if (response.toString() == "true") {
                return true;
            }
            in.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }


}
