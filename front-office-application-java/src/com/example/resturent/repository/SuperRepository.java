package com.example.resturent.repository;

import java.util.ArrayList;

public interface SuperRepository<T> {
    public boolean add(T t) throws Exception;

    public boolean update(T t) throws Exception;

    public T find(String id) throws Exception;

    public ArrayList<T> getAll() throws Exception;

    public boolean remove(String id) throws Exception;
}
