package com.example.resturent.repository;

import com.example.resturent.dto.CustomerDTO;
import com.example.resturent.exception.CheckDetailsException;
import com.example.resturent.exception.CustomerNotFound;
import com.example.resturent.util.Print;
import com.example.resturent.util.UrlSelector;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import java.io.*;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

public class CustomerRepository implements SuperRepository<CustomerDTO> {
    private static String CustomerUrl = UrlSelector.getBaseUrl() + "user";
    private static int responseCode;


    @Override
    public boolean add(CustomerDTO customerDTO) throws Exception {
        String query_url = CustomerUrl + "/add";
        String json = "{" +
                "\"customerId\":\"" + 1 + "\"," +
                "\"firstName\":\"" + customerDTO.getFirstName() + "\"," +
                "\"lastName\":\"" + customerDTO.getLastName() + "\"," +
                "\"tp\":\"" + customerDTO.getTp() + "\"," +
                "\"email\" :\"" + customerDTO.getEmail() + "\"," +
                "\"id\":\"" + customerDTO.getId() + "\"," +
                "\"idType\":\"" + customerDTO.getIdType() + "\"," +
                "\"gender\":\"" + customerDTO.getGender() + "\"," +
                "\"createdAt\":\"" + new Date() + "\"," +
                "\"isChecking\":\"" + true + "\"," +
                "\"status\":\"" + 1 + "\"}";
        try {
            URL url = new URL(query_url);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(5000);
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod("POST");
            OutputStream os = conn.getOutputStream();
            os.write(json.getBytes("UTF-8"));
            os.close();
            Print.info("Send :" + url + " Data:" + json);
            InputStream in = new BufferedInputStream(conn.getInputStream());
            String result = IOUtils.toString(in, "UTF-8");
            if (result == "true") {
                return true;
            }
            System.out.println("result after Reading JSON Response");
            JSONObject myResponse = new JSONObject(result);
            System.out.println("Json Object: " + myResponse);
            in.close();
            conn.disconnect();
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
        return false;
    }

    @Override
    public boolean update(CustomerDTO customerDTO) throws Exception {
        String query_url = CustomerUrl + "/update";
        String json = "{" +
                "\"customerId\":\"" + customerDTO.getCustomerId() + "\"," +
                "\"firstName\":\"" + customerDTO.getFirstName() + "\"," +
                "\"lastName\":\"" + customerDTO.getLastName() + "\"," +
                "\"tp\":\"" + customerDTO.getTp() + "\"," +
                "\"email\" :\"" + customerDTO.getEmail() + "\"," +
                "\"id\":\"" + customerDTO.getId() + "\"," +
                "\"idType\":\"" + customerDTO.getIdType() + "\"," +
                "\"gender\":\"" + customerDTO.getGender() + "\"," +
                "\"createdAt\":\"" + new Date() + "\"," +
                "\"isChecking\":\"" + true + "\"," +
                "\"status\":\"" + 1 + "\"}";
        try {
            URL url = new URL(query_url);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(5000);
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod("POST");
            OutputStream os = conn.getOutputStream();
            os.write(json.getBytes("UTF-8"));
            os.close();
            Print.info("Send :" + url + " Data:" + json);
            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            String result = IOUtils.toString(in, "UTF-8");
            if (result == "true") {
                return true;
            }
            System.out.println("result after Reading JSON Response");
            JSONObject myResponse = new JSONObject(result);
            System.out.println("Json Object: " + myResponse);
            in.close();
            conn.disconnect();
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
        return false;
    }

    @Override
    public CustomerDTO find(String id) throws CheckDetailsException, CustomerNotFound {
        int code = 0;
        String query_url = CustomerUrl + "/findCustomerByCustId/" + id;
        try {
            URL obj = null;

            obj = new URL(query_url);

            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            responseCode = con.getResponseCode();
            code = responseCode;
            System.out.println("\nSending 'GET' request to URL : " + query_url);
            System.out.println("Response Code : " + responseCode);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            System.out.println(response);

            Gson gson = new Gson();
            CustomerDTO customerDTO = gson.fromJson(response.toString(), CustomerDTO.class);

            in.close();
            return customerDTO;
        } catch (IOException e) {
            if (responseCode == 500) {
                throw new CustomerNotFound();
            } else {
                throw new CheckDetailsException();
            }
        }

    }

    @Override
    public ArrayList<CustomerDTO> getAll() throws Exception {
        ArrayList<CustomerDTO> sendCustomerDTOS = new ArrayList<>();
        String query_url = CustomerUrl + "/";
        try {
            URL obj = null;
            obj = new URL(query_url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + query_url);
            System.out.println("Response Code : " + responseCode);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            System.out.println(response);
            Gson gson = new Gson();
            Type collectionType = new TypeToken<Collection<CustomerDTO>>() {
            }.getType();
            Collection<CustomerDTO> enums = gson.fromJson(response.toString(), collectionType);
            for (CustomerDTO c :
                    enums) {
                sendCustomerDTOS.add(c);
            }
            in.close();
            return sendCustomerDTOS;
        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }

    @Override
    public boolean remove(String id) throws Exception {


        String query_url = CustomerUrl + "/remove/" + id;
        try {
            URL obj = null;

            obj = new URL(query_url);

            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + query_url);
            System.out.println("Response Code : " + responseCode);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            if (response.toString() == "true") {
                return true;
            }
            in.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }
}
