package com.example.resturent.dto;

import java.util.ArrayList;

public class BookingReturnDTO {
    private CustomerDTO customerDTO;
    private ArrayList<RoomRegistryDTO> roomRegistryDTOS;
    private ArrayList<RoomDTO> roomDTOS;

    public BookingReturnDTO() {
    }

    public BookingReturnDTO(CustomerDTO customerDTO, ArrayList<RoomRegistryDTO> roomRegistryDTOS, ArrayList<RoomDTO> roomDTOS) {
        this.customerDTO = customerDTO;
        this.roomRegistryDTOS = roomRegistryDTOS;
        this.roomDTOS = roomDTOS;
    }

    public CustomerDTO getCustomerDTO() {
        return customerDTO;
    }

    public void setCustomerDTO(CustomerDTO customerDTO) {
        this.customerDTO = customerDTO;
    }

    public ArrayList<RoomRegistryDTO> getRoomRegistryDTOS() {
        return roomRegistryDTOS;
    }

    public void setRoomRegistryDTOS(ArrayList<RoomRegistryDTO> roomRegistryDTOS) {
        this.roomRegistryDTOS = roomRegistryDTOS;
    }

    public ArrayList<RoomDTO> getRoomDTOS() {
        return roomDTOS;
    }

    public void setRoomDTOS(ArrayList<RoomDTO> roomDTOS) {
        this.roomDTOS = roomDTOS;
    }

    @Override
    public String toString() {
        return "BookingReturnDTO{" +
                "customerDTO=" + customerDTO +
                ", roomRegistryDTOS=" + roomRegistryDTOS +
                ", roomDTOS=" + roomDTOS +
                '}';
    }
}
