package com.example.resturent.dto;

public class RoomDTO extends SuperDTO {
    private String roomNumber;
    private String roomType;
    private String password;
    private int bedCount;
    private double price;
    private int guest;
    private int userId;
    private int status;
    private String createdAt;

    public RoomDTO() {
    }

    public RoomDTO(String roomNumber, String roomType, String password, int bedCount, double price, int guest, int userId, int status, String createdAt) {
        this.roomNumber = roomNumber;
        this.roomType = roomType;
        this.password = password;
        this.bedCount = bedCount;
        this.price = price;
        this.guest = guest;
        this.userId = userId;
        this.status = status;
        this.createdAt = createdAt;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getBedCount() {
        return bedCount;
    }

    public void setBedCount(int bedCount) {
        this.bedCount = bedCount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getGuest() {
        return guest;
    }

    public void setGuest(int guest) {
        this.guest = guest;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "RoomDTO{" +
                "roomNumber='" + roomNumber + '\'' +
                ", roomType='" + roomType + '\'' +
                ", password='" + password + '\'' +
                ", bedCount=" + bedCount +
                ", price=" + price +
                ", guest=" + guest +
                ", userId=" + userId +
                ", status=" + status +
                ", createdAt='" + createdAt + '\'' +
                '}';
    }
}
