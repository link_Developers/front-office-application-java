package com.example.resturent.dto;

public class ItemDTO extends SuperDTO {
    private int itemId;
    private String itemName;
    private String itemDec;
    private double price;
    private String createdAt;
    private int status;
    private String file_path;
    private String imageName;
    private String file_extension;
    private String imageUrl;

    public ItemDTO() {
    }

    public ItemDTO(int itemId, String itemName, String itemDec, double price, String createdAt, int status, String file_path, String imageName, String file_extension, String imageUrl) {
        this.itemId = itemId;
        this.itemName = itemName;
        this.itemDec = itemDec;
        this.price = price;
        this.createdAt = createdAt;
        this.status = status;
        this.file_path = file_path;
        this.imageName = imageName;
        this.file_extension = file_extension;
        this.imageUrl = imageUrl;
    }

    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getFile_extension() {
        return file_extension;
    }

    public void setFile_extension(String file_extension) {
        this.file_extension = file_extension;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemDec() {
        return itemDec;
    }

    public void setItemDec(String itemDec) {
        this.itemDec = itemDec;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ItemDTO{" +
                "itemId=" + itemId +
                ", itemName='" + itemName + '\'' +
                ", itemDec='" + itemDec + '\'' +
                ", price=" + price +
                ", createdAt=" + createdAt +
                ", status=" + status +
                '}';
    }
}
