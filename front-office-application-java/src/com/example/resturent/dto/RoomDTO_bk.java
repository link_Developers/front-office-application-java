package com.example.resturent.dto;

public class RoomDTO_bk {
    private int id;
    private String roomNumber;

    public RoomDTO_bk() {
    }

    public RoomDTO_bk(int id, String roomNumber) {
        this.id = id;
        this.roomNumber = roomNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    @Override
    public String toString() {
        return "RoomDTO{" +
                "id=" + id +
                ", RoomNumber='" + roomNumber + '\'' +
                '}';
    }
}
