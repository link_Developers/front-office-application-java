package com.example.resturent.dto;

import java.util.List;

public class BookingDTO extends SuperDTO {
    private int userId;
    private String bookingMethod;
    private List<RoomDTO> rooms;

    public BookingDTO() {
    }

    public BookingDTO(int userId, String bookingMethod, List<RoomDTO> rooms) {
        this.userId = userId;
        this.bookingMethod = bookingMethod;
        this.rooms = rooms;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getBookingMethod() {
        return bookingMethod;
    }

    public void setBookingMethod(String bookingMethod) {
        this.bookingMethod = bookingMethod;
    }

    public List<RoomDTO> getRooms() {
        return rooms;
    }

    public void setRooms(List<RoomDTO> rooms) {
        this.rooms = rooms;
    }

    @Override
    public String toString() {
        return "BookingDTO{" +
                "userId=" + userId +
                ", bookingMethod='" + bookingMethod + '\'' +
                ", rooms=" + rooms +
                '}';
    }
}
