package com.example.resturent.dto;

public class PaymentDTO extends SuperDTO {
    private int invoice;
    private int user_id;
    private double cash;
    private double total;
    private double balance;
    private String createdDateAndTime;

    public PaymentDTO() {
    }

    public PaymentDTO(int invoice, int user_id, double cash, double total, double balance, String createdDateAndTime) {
        this.invoice = invoice;
        this.user_id = user_id;
        this.cash = cash;
        this.total = total;
        this.balance = balance;
        this.createdDateAndTime = createdDateAndTime;
    }

    public int getInvoice() {
        return invoice;
    }

    public void setInvoice(int invoice) {
        this.invoice = invoice;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public double getCash() {
        return cash;
    }

    public void setCash(double cash) {
        this.cash = cash;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getCreatedDateAndTime() {
        return createdDateAndTime;
    }

    public void setCreatedDateAndTime(String createdDateAndTime) {
        this.createdDateAndTime = createdDateAndTime;
    }

    @Override
    public String toString() {
        return "PaymentDTO{" +
                "invoice=" + invoice +
                ", user_id=" + user_id +
                ", cash=" + cash +
                ", total=" + total +
                ", balance=" + balance +
                ", createdDateAndTime='" + createdDateAndTime + '\'' +
                '}';
    }
}
