package com.example.resturent.dto;

public class MajorCateDTO extends SuperDTO {
    private int cate_id;
    private String cate_name;
    private String created_at;
    private int status;

    public MajorCateDTO() {
    }

    public MajorCateDTO(int cate_id, String cate_name, String created_at, int status) {
        this.cate_id = cate_id;
        this.cate_name = cate_name;
        this.created_at = created_at;
        this.status = status;
    }

    public int getCate_id() {
        return cate_id;
    }

    public void setCate_id(int cate_id) {
        this.cate_id = cate_id;
    }

    public String getCate_name() {
        return cate_name;
    }

    public void setCate_name(String cate_name) {
        this.cate_name = cate_name;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "MajorCateDTO{" +
                "cate_id=" + cate_id +
                ", cate_name='" + cate_name + '\'' +
                ", created_at='" + created_at + '\'' +
                ", status=" + status +
                '}';
    }
}

