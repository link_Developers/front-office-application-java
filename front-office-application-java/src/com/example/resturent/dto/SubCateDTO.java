package com.example.resturent.dto;

public class SubCateDTO extends SuperDTO {
    private int sub_cate_id;
    private String sub_cate_name;
    private int cate_id;
    private String created_at;
    private int status;

    public SubCateDTO() {
    }

    public SubCateDTO(int sub_cate_id, String sub_cate_name, int cate_id, String created_at, int status) {
        this.sub_cate_id = sub_cate_id;
        this.sub_cate_name = sub_cate_name;
        this.cate_id = cate_id;
        this.created_at = created_at;
        this.status = status;
    }

    public int getSub_cate_id() {
        return sub_cate_id;
    }

    public void setSub_cate_id(int sub_cate_id) {
        this.sub_cate_id = sub_cate_id;
    }

    public String getSub_cate_name() {
        return sub_cate_name;
    }

    public void setSub_cate_name(String sub_cate_name) {
        this.sub_cate_name = sub_cate_name;
    }

    public int getCate_id() {
        return cate_id;
    }

    public void setCate_id(int cate_id) {
        this.cate_id = cate_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "SubCateDTO{" +
                "sub_cate_id=" + sub_cate_id +
                ", sub_cate_name='" + sub_cate_name + '\'' +
                ", cate_id=" + cate_id +
                ", created_at='" + created_at + '\'' +
                ", status=" + status +
                '}';
    }
}
