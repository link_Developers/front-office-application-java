package com.example.resturent.dto;

import java.sql.Date;

public class OrderDTO extends SuperDTO {
    private int oid;
    private String roomNo;
    private int itemId;
    private int qty;
    private double total;
    private Date createdDateAndTime;
    private int orderStatus;

    public OrderDTO() {
    }

    public OrderDTO(int oid, String roomNo, int itemId, int qty, double total, Date createdDateAndTime, int orderStatus) {
        this.oid = oid;
        this.roomNo = roomNo;
        this.itemId = itemId;
        this.qty = qty;
        this.total = total;
        this.createdDateAndTime = createdDateAndTime;
        this.orderStatus = orderStatus;
    }

    public int getOid() {
        return oid;
    }

    public void setOid(int oid) {
        this.oid = oid;
    }

    public String getRoomNo() {
        return roomNo;
    }

    public void setRoomNo(String roomNo) {
        this.roomNo = roomNo;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public Date getCreatedDateAndTime() {
        return createdDateAndTime;
    }

    public void setCreatedDateAndTime(Date createdDateAndTime) {
        this.createdDateAndTime = createdDateAndTime;
    }

    public int getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(int orderStatus) {
        this.orderStatus = orderStatus;
    }

    @Override
    public String toString() {
        return "OrderDTO{" +
                "oid=" + oid +
                ", roomNo='" + roomNo + '\'' +
                ", itemId=" + itemId +
                ", qty=" + qty +
                ", total=" + total +
                ", createdDateAndTime=" + createdDateAndTime +
                ", orderStatus=" + orderStatus +
                '}';
    }
}
