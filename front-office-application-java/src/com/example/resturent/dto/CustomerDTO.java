package com.example.resturent.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomerDTO extends SuperDTO {

    @JsonProperty("tp")
    public String tp;
    @JsonProperty("email")
    public String email;
    @JsonProperty("id")
    public String id;
    @JsonProperty("idType")
    public String idType;
    @JsonProperty("gender")
    public String gender;
    @JsonProperty("createdAt")
    public String createdAt;
    @JsonProperty("isChecking")
    public boolean isChecking;
    @JsonProperty("customerId")
    private int customerId;
    @JsonProperty("firstName")
    private String firstName;
    @JsonProperty("lastName")
    private String lastName;
    @JsonProperty("status")
    private int status;


    public CustomerDTO() {
    }

    public CustomerDTO(int customerId, String firstName, String lastName, String tp, String email, String id, String idType, String gender, String createdAt, boolean isChecking, int status) {
        this.customerId = customerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.tp = tp;
        this.email = email;
        this.id = id;
        this.idType = idType;
        this.gender = gender;
        this.createdAt = createdAt;
        this.isChecking = isChecking;
        this.status = status;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTp() {
        return tp;
    }

    public void setTp(String tp) {
        this.tp = tp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public boolean isChecking() {
        return isChecking;
    }

    public void setChecking(boolean checking) {
        isChecking = checking;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "CustomerDTO{" +
                "customerId=" + customerId +
                ", firstName='" + firstName + '\'' +
                ", LastName='" + lastName + '\'' +
                ", tp='" + tp + '\'' +
                ", Email='" + email + '\'' +
                ", id='" + id + '\'' +
                ", idType='" + idType + '\'' +
                ", gender='" + gender + '\'' +
                ", CreatedAt=" + createdAt +
                ", isChecking=" + isChecking +
                ", status=" + status +
                '}';
    }
}
