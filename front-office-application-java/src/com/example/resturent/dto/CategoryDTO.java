package com.example.resturent.dto;

import java.util.List;

public class CategoryDTO extends SuperDTO {
    private MajorCateDTO majorCateDTO;
    private List<SubCateDTO> subCateDTOS;

    public CategoryDTO() {
    }

    public CategoryDTO(MajorCateDTO majorCateDTO, List<SubCateDTO> subCateDTOS) {
        this.majorCateDTO = majorCateDTO;
        this.subCateDTOS = subCateDTOS;
    }

    public MajorCateDTO getMajorCateDTO() {
        return majorCateDTO;
    }

    public void setMajorCateDTO(MajorCateDTO majorCateDTO) {
        this.majorCateDTO = majorCateDTO;
    }

    public List<SubCateDTO> getSubCateDTOS() {
        return subCateDTOS;
    }

    public void setSubCateDTOS(List<SubCateDTO> subCateDTOS) {
        this.subCateDTOS = subCateDTOS;
    }

    @Override
    public String toString() {
        return "CategoryDTO{" +
                "majorCateDTO=" + majorCateDTO +
                ", subCateDTOS=" + subCateDTOS +
                '}';
    }
}
