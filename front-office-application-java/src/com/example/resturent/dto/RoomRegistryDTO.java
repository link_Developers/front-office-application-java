package com.example.resturent.dto;

public class RoomRegistryDTO extends SuperDTO {
    private int rr_id;
    private String room_no;
    private String checkInDateAndTime;
    private String checkOutDateAndTime;
    private String checkingMethod;
    private double currentTotal;
    private int days;
    private int status;

    public RoomRegistryDTO() {
    }

    public RoomRegistryDTO(int rr_id, String room_no, String checkInDateAndTime, String checkOutDateAndTime, String checkingMethod, double currentTotal, int days, int status) {
        this.rr_id = rr_id;
        this.room_no = room_no;
        this.checkInDateAndTime = checkInDateAndTime;
        this.checkOutDateAndTime = checkOutDateAndTime;
        this.checkingMethod = checkingMethod;
        this.currentTotal = currentTotal;
        this.days = days;
        this.status = status;
    }

    public int getRr_id() {
        return rr_id;
    }

    public void setRr_id(int rr_id) {
        this.rr_id = rr_id;
    }

    public String getRoom_no() {
        return room_no;
    }

    public void setRoom_no(String room_no) {
        this.room_no = room_no;
    }

    public String getCheckInDateAndTime() {
        return checkInDateAndTime;
    }

    public void setCheckInDateAndTime(String checkInDateAndTime) {
        this.checkInDateAndTime = checkInDateAndTime;
    }

    public String getCheckOutDateAndTime() {
        return checkOutDateAndTime;
    }

    public void setCheckOutDateAndTime(String checkOutDateAndTime) {
        this.checkOutDateAndTime = checkOutDateAndTime;
    }

    public String getCheckingMethod() {
        return checkingMethod;
    }

    public void setCheckingMethod(String checkingMethod) {
        this.checkingMethod = checkingMethod;
    }

    public double getCurrentTotal() {
        return currentTotal;
    }

    public void setCurrentTotal(double currentTotal) {
        this.currentTotal = currentTotal;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "RoomRegistryDTO{" +
                "rr_id=" + rr_id +
                ", room_no='" + room_no + '\'' +
                ", checkInDateAndTime=" + checkInDateAndTime +
                ", checkOutDateAndTime=" + checkOutDateAndTime +
                ", checkingMethod='" + checkingMethod + '\'' +
                ", currentTotal=" + currentTotal +
                ", days=" + days +
                ", status=" + status +
                '}';
    }
}
