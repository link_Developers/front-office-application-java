package com.example.resturent.dto;

public class LoginDTO extends SuperDTO {
    private String roomNumber;
    private String password;
    private String ipAddress;

    public LoginDTO() {
    }

    public LoginDTO(String roomNumber, String password, String ipAddress) {
        this.roomNumber = roomNumber;
        this.password = password;
        this.ipAddress = ipAddress;
    }

    @Override
    public String toString() {
        return "LoginDTO{" +
                "roomNumber='" + roomNumber + '\'' +
                ", password='" + password + '\'' +
                ", ipAddress='" + ipAddress + '\'' +
                '}';
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }
}
