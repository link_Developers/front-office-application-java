package com.example.resturent.view.model.table;

public class CustomerTableModel {
    private String userId;
    private String firstName;
    private String lastName;
    private String tp;
    private String email;
    private String nic_passport;
    private String gender;
    private String createdAt;

    public CustomerTableModel() {
    }

    public CustomerTableModel(String userId, String firstName, String lastName, String tp, String email, String nic_passport, String gender, String createdAt) {
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.tp = tp;
        this.email = email;
        this.nic_passport = nic_passport;
        this.gender = gender;
        this.createdAt = createdAt;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTp() {
        return tp;
    }

    public void setTp(String tp) {
        this.tp = tp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNic_passport() {
        return nic_passport;
    }

    public void setNic_passport(String nic_passport) {
        this.nic_passport = nic_passport;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "CustomerTableModel{" +
                "userId='" + userId + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", tp='" + tp + '\'' +
                ", email='" + email + '\'' +
                ", nic_passport='" + nic_passport + '\'' +
                ", gender='" + gender + '\'' +
                ", createdAt='" + createdAt + '\'' +
                '}';
    }
}
