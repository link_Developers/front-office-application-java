package com.example.resturent.view.model.table;

import javafx.scene.control.CheckBox;

public class RoomTableModeForPayment {
    private CheckBox select;
    private String roomNumber;
    private String chekingDate;
    private String price;
    private String days;
    private String total;

    public RoomTableModeForPayment() {
    }

    public RoomTableModeForPayment(CheckBox select, String roomNumber, String chekingDate, String price, String days, String total) {
        this.select = select;
        this.roomNumber = roomNumber;
        this.chekingDate = chekingDate;
        this.price = price;
        this.days = days;
        this.total = total;
    }

    public CheckBox getSelect() {
        return select;
    }

    public void setSelect(CheckBox select) {
        this.select = select;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getChekingDate() {
        return chekingDate;
    }

    public void setChekingDate(String chekingDate) {
        this.chekingDate = chekingDate;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}
