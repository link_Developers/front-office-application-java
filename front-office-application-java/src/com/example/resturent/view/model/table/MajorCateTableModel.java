package com.example.resturent.view.model.table;

import javafx.scene.control.Button;

public class MajorCateTableModel {
    private String majorCateId;
    private String majorCateName;
    private Button majorCateDelete;
    private Button majorCateEdit;

    public MajorCateTableModel() {
    }

    public MajorCateTableModel(String majorCateId, String majorCateName, Button majorCateDelete, Button majorCateEdit) {
        this.majorCateId = majorCateId;
        this.majorCateName = majorCateName;
        this.majorCateDelete = majorCateDelete;
        this.majorCateEdit = majorCateEdit;
    }

    public String getMajorCateId() {
        return majorCateId;
    }

    public void setMajorCateId(String majorCateId) {
        this.majorCateId = majorCateId;
    }

    public String getMajorCateName() {
        return majorCateName;
    }

    public void setMajorCateName(String majorCateName) {
        this.majorCateName = majorCateName;
    }

    public Button getMajorCateDelete() {
        return majorCateDelete;
    }

    public void setMajorCateDelete(Button majorCateDelete) {
        this.majorCateDelete = majorCateDelete;
    }

    public Button getMajorCateEdit() {
        return majorCateEdit;
    }

    public void setMajorCateEdit(Button majorCateEdit) {
        this.majorCateEdit = majorCateEdit;
    }

    @Override
    public String toString() {
        return "MajorCateTableModel{" +
                "mid='" + majorCateId + '\'' +
                ", mName='" + majorCateName + '\'' +
                ", mDelete=" + majorCateDelete +
                ", mEdit=" + majorCateEdit +
                '}';
    }
}
