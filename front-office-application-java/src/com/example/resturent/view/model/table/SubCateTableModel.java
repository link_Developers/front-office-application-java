package com.example.resturent.view.model.table;

import javafx.scene.control.Button;

public class SubCateTableModel {

    private String subCateId;


    private String subCateName;


    private Button subCateDelete;


    private Button subCateEdit;

    public SubCateTableModel() {
    }

    public SubCateTableModel(String subCateId, String subCateName, Button subCateDelete, Button subCateEdit) {
        this.subCateId = subCateId;
        this.subCateName = subCateName;
        this.subCateDelete = subCateDelete;
        this.subCateEdit = subCateEdit;
    }

    public String getSubCateId() {
        return subCateId;
    }

    public void setSubCateId(String subCateId) {
        this.subCateId = subCateId;
    }

    public String getSubCateName() {
        return subCateName;
    }

    public void setSubCateName(String subCateName) {
        this.subCateName = subCateName;
    }

    public Button getSubCateDelete() {
        return subCateDelete;
    }

    public void setSubCateDelete(Button subCateDelete) {
        this.subCateDelete = subCateDelete;
    }

    public Button getSubCateEdit() {
        return subCateEdit;
    }

    public void setSubCateEdit(Button subCateEdit) {
        this.subCateEdit = subCateEdit;
    }

    @Override
    public String toString() {
        return "SubCateTableModel{" +
                "subCateId='" + subCateId + '\'' +
                ", subCateName='" + subCateName + '\'' +
                ", subCateDelete=" + subCateDelete +
                ", subCateEdit=" + subCateEdit +
                '}';
    }
}
