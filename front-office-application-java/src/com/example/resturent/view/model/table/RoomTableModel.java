package com.example.resturent.view.model.table;

public class RoomTableModel {
    private String roomNumber;
    private String roomType;
    private String bedCount;
    private String price;
    private String gustId;
    private String status;

    public RoomTableModel() {
    }

    public RoomTableModel(String roomNumber, String roomType, String bedCount, String price, String gustId, String status) {
        this.roomNumber = roomNumber;
        this.roomType = roomType;
        this.bedCount = bedCount;
        this.price = price;
        this.gustId = gustId;
        this.status = status;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public String getBedCount() {
        return bedCount;
    }

    public void setBedCount(String bedCount) {
        this.bedCount = bedCount;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getGustId() {
        return gustId;
    }

    public void setGustId(String gustId) {
        this.gustId = gustId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "RoomTableModel{" +
                "roomNumber='" + roomNumber + '\'' +
                ", roomType='" + roomType + '\'' +
                ", bedCount='" + bedCount + '\'' +
                ", price='" + price + '\'' +
                ", gustId='" + gustId + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
