package com.example.resturent.view.model.chBoxModel;

import com.example.resturent.dto.CustomerDTO;

public class CustomerModel {
    private String customerName;
    private CustomerDTO customerDTO;

    public CustomerModel() {
    }

    public CustomerModel(String customerName, CustomerDTO customerDTO) {
        this.customerName = customerName;
        this.customerDTO = customerDTO;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public CustomerDTO getCustomerDTO() {
        return customerDTO;
    }

    public void setCustomerDTO(CustomerDTO customerDTO) {
        this.customerDTO = customerDTO;
    }

    @Override
    public String toString() {
        return this.customerName;
    }
}
