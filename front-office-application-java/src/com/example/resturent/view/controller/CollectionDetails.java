package com.example.resturent.view.controller;

import com.example.resturent.controller.BookingManageController;
import com.example.resturent.dto.BookingReturnDTO;
import com.example.resturent.dto.CustomerDTO;
import com.example.resturent.dto.RoomDTO;
import com.example.resturent.dto.RoomRegistryDTO;
import com.example.resturent.util.PageValidator;
import com.example.resturent.util.TempDB;
import com.example.resturent.view.model.table.RoomTableModeForPayment;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.HashMap;

public class CollectionDetails extends SuperController {
    @FXML
    private TextField txtSearch;

    @FXML
    private Button btnSearch;

    @FXML
    private ToggleButton btnShoFilter;

    @FXML
    private RadioButton rbByCustomer;

    @FXML
    private ToggleGroup filter;

    @FXML
    private RadioButton rbByRoom;

    @FXML
    private RadioButton rbnByRes;

    @FXML
    private TableView<RoomTableModeForPayment> tblRoomTable1;

    @FXML
    private TableColumn<RoomTableModeForPayment, CheckBox> select;

    @FXML
    private TableColumn<RoomTableModeForPayment, String> roomNumber;

    @FXML
    private TableColumn<RoomTableModeForPayment, String> chekingDate;

    @FXML
    private TableColumn<RoomTableModeForPayment, String> price;

    @FXML
    private TableColumn<RoomTableModeForPayment, String> days;

    @FXML
    private TableColumn<RoomTableModeForPayment, String> total;

    @FXML
    private TableColumn<RoomTableModeForPayment, String> status;

    @FXML
    private Label txtCustId;

    @FXML
    private Label txtCustName;

    @FXML
    private Label txtContactNo;

    @FXML
    private Label txtEmail;

    @FXML
    private Label txtPaymentMethod;

    @FXML
    private Label txtNicPassport;

    @FXML
    private ToggleButton btnNext;

    @FXML
    private ToggleButton btnNext1;

    @FXML
    private Label txtTotal;
    @FXML
    private HBox filterPane;
    private BookingManageController bookingManageController;
    private BookingReturnDTO bookingReturnDTO;
    private double totalPrice;
    private HashMap<String, RoomDTO> roomDTOHashMap = new HashMap<>();
    private HashMap<String, RoomRegistryDTO> registryDTOHashMap = new HashMap<>();

    @FXML
    void btnShoFilter(ActionEvent event) {
        filterPane.setVisible(btnShoFilter.isSelected());

    }

    @FXML
    void txtSearchAction(ActionEvent event) {
        try {
            if (txtSearch.getText() != "") {
                if (rbByCustomer.isSelected()) {
                    bookingReturnDTO = bookingManageController.getBookingByCustomerId(txtSearch.getText());
                    SetCustomerDetails(bookingReturnDTO.getCustomerDTO(), bookingReturnDTO.getRoomRegistryDTOS().get(1).getCheckingMethod());
                    loadTable();

                } else if (rbByRoom.isSelected()) {
                    bookingReturnDTO = bookingManageController.getBookingByRoomId(txtSearch.getText());

                    SetCustomerDetails(bookingReturnDTO.getCustomerDTO(), bookingReturnDTO.getRoomRegistryDTOS().get(1).getCheckingMethod());
                    loadTable();
                }
            }
        } catch (NullPointerException e) {
            new Alert(Alert.AlertType.ERROR, "Check Details").show();
        }
    }

    private void SetCustomerDetails(CustomerDTO customerDTO, String CheckinMethod) {

        txtCustId.setText(customerDTO.getCustomerId() + "");
        txtContactNo.setText(customerDTO.getTp());
        txtCustName.setText(customerDTO.getFirstName() + " " + customerDTO.getLastName());
        txtEmail.setText(customerDTO.getEmail());
        txtNicPassport.setText(customerDTO.getId());
        txtPaymentMethod.setText(CheckinMethod);
    }

    @Override
    void refresh() {

    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        bookingManageController = new BookingManageController();
        select.setCellValueFactory(new PropertyValueFactory<>("select"));
        roomNumber.setCellValueFactory(new PropertyValueFactory<>("roomNumber"));
        chekingDate.setCellValueFactory(new PropertyValueFactory<>("chekingDate"));
        price.setCellValueFactory(new PropertyValueFactory<>("price"));
        days.setCellValueFactory(new PropertyValueFactory<>("days"));
        total.setCellValueFactory(new PropertyValueFactory<>("total"));
        btnShoFilter.setSelected(false);

    }

    private void loadTable() {
        ObservableList<RoomTableModeForPayment> objects = FXCollections.observableArrayList();

        for (RoomDTO rd : bookingReturnDTO.getRoomDTOS()) {
            roomDTOHashMap.put(rd.getRoomNumber(), rd);

        }
        for (RoomRegistryDTO rr : bookingReturnDTO.getRoomRegistryDTOS()) {
            registryDTOHashMap.put(rr.getRoom_no(), rr);
            CheckBox checkBox = new CheckBox("");
            checkBox.setSelected(true);
            checkBox.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    tblRoomMouseClick();
                }
            });
            double totalPrice = roomDTOHashMap.get(rr.getRoom_no()).getPrice() * rr.getDays();
            this.totalPrice = this.totalPrice + totalPrice;
            objects.add(
                    new RoomTableModeForPayment(
                            checkBox,
                            rr.getRoom_no(),
                            rr.getCheckInDateAndTime(),
                            roomDTOHashMap.get(rr.getRoom_no()).getPrice() + "",
                            rr.getDays() + "",
                            totalPrice + ""
                    ));
        }
        txtTotal.setText(this.totalPrice + "");
        tblRoomTable1.setItems(objects);
    }

    private void tblRoomMouseClick() {
        double dynamicTotal = 0;
        for (int i = 0; i < tblRoomTable1.getItems().size(); i++) {
            if (tblRoomTable1.getItems().get(i).getSelect().isSelected()) {
                dynamicTotal = dynamicTotal + Double.parseDouble(tblRoomTable1.getItems().get(i).getTotal());
            }
        }
        this.totalPrice = dynamicTotal;
        txtTotal.setText(this.totalPrice + "");
    }

    public void btnSaveAction(ActionEvent actionEvent) {
        ArrayList<RoomDTO> roomDTOS = new ArrayList<>();
        ArrayList<RoomRegistryDTO> registryDTOS = new ArrayList<>();
        for (String rd :
                roomDTOHashMap.keySet()) {
            roomDTOS.add(roomDTOHashMap.get(rd));
        }
        for (String rd :
                registryDTOHashMap.keySet()) {
            registryDTOS.add(registryDTOHashMap.get(rd));
        }
        TempDB.setBookingReturnDTO(new BookingReturnDTO(
                bookingReturnDTO.getCustomerDTO(),
                registryDTOS,
                roomDTOS
        ));
        PageValidator.setCollectDetailsIsDone(true);
    }

    public void tblRoomMouseClick(MouseEvent mouseEvent) {

    }
}
