/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.example.resturent.view.controller;

import animatefx.animation.FadeInUpBig;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.BoxBlur;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.IOException;


public class HomePageController extends SuperController {
    static HomePageController homePageController;
    @FXML
    private Label txtTitle;
    @FXML
    private BorderPane borderPane;
    @FXML
    private Circle btnMini;
    @FXML
    private Circle btnExit;
    @FXML
    private Circle btnMax;
    @FXML
    private VBox btnVPanel;
    @FXML
    private AnchorPane titleBar;
    @FXML
    private Pane mainButtonPanel;
    @FXML
    private Button btnDashboard;
    @FXML
    private Button btnBooking;
    @FXML
    private Button btnCustomer;
    @FXML
    private Button btnItem;
    @FXML
    private Pane moveBar;
    @FXML
    private Button btnRoom;
    @FXML
    private Button btnPayment;
    @FXML
    private Button btnReport;
    @FXML
    private Button btnNotification;
    @FXML
    private Button btnSettings;
    private double xOffset = 0;
    private double yOffset = 0;
    private Stage stage;

    public HomePageController() {
//        ChangeBackground(btnDashboard);
    }

    public static HomePageController getInstance() {
        return homePageController;
    }

    public Button getBtnCustomer() {
        return this.btnCustomer;
    }

    public Button getBtnBooking() {
        return this.btnBooking;
    }

    @Override
    public void init() throws Exception {
        super.init();
        ;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        homePageController = this;
        stage = primaryStage;
        moveBar.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });
        moveBar.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                primaryStage.setX(event.getScreenX() - xOffset);
                primaryStage.setY(event.getScreenY() - yOffset);
            }
        });
        PayAnimation();
        ChangeBackground(btnPayment);
    }

    private void PayAnimation() throws InterruptedException {
        FadeInUpBig fadeInUpBig = new FadeInUpBig(btnVPanel);
        fadeInUpBig.setDelay(new Duration(1000));

    }

    @FXML
    void btnMacClick(MouseEvent event) {
        boolean t = false;
        Stage stage = (Stage) btnMax.getScene().getWindow();
        if (stage.isMaximized()) {
            stage.setMaximized(false);
        } else {
            stage.setMaximized(true);
        }
    }

    @FXML
    void btnMiniClick(MouseEvent event) {

        Stage stage = (Stage) btnMini.getScene().getWindow();
        stage.setIconified(true);
    }

    @FXML
    void systemExit(MouseEvent event) {
        System.exit(0);
    }

    @FXML
    void btnNotificationAction(ActionEvent event) {
        ChangeBackground(btnNotification);
    }

    @FXML
    void btnPaymentAction(ActionEvent event) {
        ChangeBackground(btnPayment);
    }

    @FXML
    void btnReportAction(ActionEvent event) {
        ChangeBackground(btnReport);
    }

    @FXML
    void btnRoomAction(ActionEvent event) {
        ChangeBackground(btnRoom);
    }

    @FXML
    void btnSettingsAction(ActionEvent event) {
        ChangeBackground(btnSettings);
    }

    @FXML
    void btnBookingAction(ActionEvent event) {
        ChangeBackground(btnBooking);
        ;
    }

    @FXML
    void btnCustomerAction(ActionEvent event) {
        ChangeBackground(btnCustomer);


    }

    @FXML
    void btnDashboardOnAction(ActionEvent event) {
        ChangeBackground(btnDashboard);

    }

    @FXML
    void btnItemAction(ActionEvent event) {
        ChangeBackground(btnItem);
    }

    @FXML
    void btnCloseMouseEnterd(MouseEvent event) {
        EnterEvent(btnExit);
    }

    @FXML
    void btnCloseMouseExit(MouseEvent event) {
        ExitEvent(btnExit);
    }

    @FXML
    void btnMaxEnterd(MouseEvent event) {
        EnterEvent(btnMax);
    }

    @FXML
    void btnMaxExit(MouseEvent event) {
        ExitEvent(btnMax);
    }

    @FXML
    void btnMiniEnterd(MouseEvent event) {
        EnterEvent(btnMini);
//        new Wobble(btnMini).play();
    }

    @FXML
    void btnMiniExit(MouseEvent event) {
        ExitEvent(btnMini);
//        new Wobble(btnMini).play();
    }

    public void ChangePanel(String name) {
        Parent root = null;
        try {
//            root = FXMLLoader.load(getClass().getResource("/com/example/resturent/view/" + name + ".fxml"));
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/com/example/resturent/view/" + name + ".fxml"));
            root = fxmlLoader.load();
            SuperController controller = fxmlLoader.getController();
            controller.start(stage);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        borderPane.setCenter(root);
    }


    public void ChangeBackground(Button btn) {
        ChangePanel(btn.getText());
        ObservableList<Node> children = btnVPanel.getChildren();
        for (Node node : btnVPanel.getChildren()) {
            if (node instanceof Button) {
                final Button button = (Button) node;
                button.setStyle(btn.getStyle());
                // all your logic
            }

        }
        txtTitle.setText(btn.getText());
        btn.setStyle("-fx-background-color: #464646");
    }

    private void EnterEvent(Node node) {

        BoxBlur boxBlur = new BoxBlur();
        node.setEffect(boxBlur);

    }

    private void ExitEvent(Node node) {
        node.setEffect(null);
    }

    @FXML
    void moveBarDrag(MouseEvent event) {


    }

    public void starNew(Stage stage) {
        try {
            this.start(stage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    void refresh() {

    }
}

