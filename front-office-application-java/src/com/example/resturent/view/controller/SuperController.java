package com.example.resturent.view.controller;

import javafx.application.Application;

public abstract class SuperController extends Application {
    abstract void refresh();
}
