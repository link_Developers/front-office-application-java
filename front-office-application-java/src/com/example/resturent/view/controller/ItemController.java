package com.example.resturent.view.controller;

import com.example.resturent.controller.CategoryController;
import com.example.resturent.controller.SubCateController;
import com.example.resturent.dto.MajorCateDTO;
import com.example.resturent.dto.SubCateDTO;
import com.example.resturent.util.CategoryStaticDB;
import com.example.resturent.view.model.table.MajorCateTableModel;
import com.example.resturent.view.model.table.SubCateTableModel;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javafx.util.Callback;

import java.io.File;
import java.util.ArrayList;


public class ItemController extends SuperController {
    Callback<TableColumn<MajorCateTableModel, String>, TableCell<MajorCateTableModel, String>> cellFactory
            =
            new Callback<TableColumn<MajorCateTableModel, String>, TableCell<MajorCateTableModel, String>>() {
                @Override
                public TableCell call(final TableColumn<MajorCateTableModel, String> param) {
                    final TableCell<MajorCateTableModel, String> cell = new TableCell<MajorCateTableModel, String>() {

                        final Button btn = new Button("edit");

                        @Override
                        public void updateItem(String item, boolean empty) {
                            super.updateItem(item, empty);
                            if (empty) {
                                setGraphic(null);
                                setText(null);
                            } else {
                                btn.setOnAction(event -> {
                                    MajorCateTableModel MajorCateTableModel = getTableView().getItems().get(getIndex());
                                    System.out.println(MajorCateTableModel.getMajorCateId()
                                            + "   " + MajorCateTableModel.getMajorCateName());
                                });
                                setGraphic(btn);
                                setText(null);
                            }
                        }
                    };
                    return cell;
                }
            };
    @FXML
    private TableView tblMajorCate;
    @FXML
    private TableColumn<?, ?> majorCateId;
    @FXML
    private TableColumn<?, ?> majorCateName;
    @FXML
    private TableColumn<?, ?> majorCateDelete;
    @FXML
    private TableColumn<?, ?> majorCateEdit;
    @FXML
    private TableView<SubCateTableModel> tblSubCate;
    @FXML
    private AnchorPane detailsPanel;
    @FXML
    private TextField txtItemName;
    @FXML
    private TextField txtDecs;
    @FXML
    private TextField txtPrice;
    @FXML
    private ImageView imageView;
    @FXML
    private ChoiceBox<String> chbMajorCate;
    @FXML
    private ChoiceBox<String> chbSubCate;
    @FXML
    private Button btnAddMejorCate;
    @FXML
    private Button btnAddSubCate;
    @FXML
    private Button btnSave;
    @FXML
    private Button btnRemove;
    @FXML
    private Button btnViewDetails;
    @FXML
    private Label txtStatusMessage;
    @FXML
    private TextField txtMajorCateFind;
    @FXML
    private ToggleGroup SearchMejor;
    @FXML
    private TextField txtSubCateFind;
    @FXML
    private ToggleGroup SearchMejor1;
    private Stage stage;
    private com.example.resturent.controller.ItemController itemController;
    private CategoryController categoryController;
    private ObservableList<MajorCateTableModel> tableModels;
    private SubCateController subCateController;
    private String SubCate;
    private String MajorCate;
    @FXML
    private TableColumn<?, ?> subCateId;
    @FXML
    private TableColumn<?, ?> subCateName;
    @FXML
    private TableColumn<?, ?> subCateDelete;
    @FXML
    private TableColumn<?, ?> subCateEdit;
    private SubCateTableModel subCateTableModel;
    private Button btnMajorDelete;
    private Button btnMajorEdit;
    private MajorCateTableModel majorCateTableModel;
    private boolean lock = false;

    @FXML
    void ImageAction(MouseEvent event) {
        try {


            File recordsDir = new File(System.getProperty("user.home"), "hotel/images");
            if (!recordsDir.exists()) {
                recordsDir.mkdirs();
            }
            FileChooser fileChooser = new FileChooser();
            FileChooser.ExtensionFilter fileExtensions =
                    new FileChooser.ExtensionFilter(
                            "Images", "*.jpg", "*.png", "*.jpge");
            fileChooser.setTitle("Select Item Image");

            fileChooser.setInitialDirectory(recordsDir);
            fileChooser.getExtensionFilters().add(fileExtensions);

            File file = fileChooser.showOpenDialog(stage);
            if (file != null) {
                Image image = new Image(file.toURI().toString());
                imageView.setImage(image);
                imageView.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        itemController.uploadImage(file, this);
                    }
                });

                System.out.println("file :" + file.toURI().toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void btnAddMejorCateAction(ActionEvent event) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/com/example/resturent/view/AddMajorCate.fxml"));
            Parent parent = fxmlLoader.load();
            AddMajorCateController dialogController = fxmlLoader.<AddMajorCateController>getController();
            Scene scene = new Scene(parent, 570, 223);
            Stage stage = new Stage();
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setScene(scene);
            stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    System.out.println("new Refresh");
                }
            });
            stage.setAlwaysOnTop(true);
            dialogController.start(stage);
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @FXML
    void btnAddSubCateAction(ActionEvent event) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/com/example/resturent/view/AddSubCate.fxml"));
            Parent parent = fxmlLoader.load();
            AddSubCateController dialogController = fxmlLoader.<AddSubCateController>getController();
            Scene scene = new Scene(parent, 570, 223);
            Stage stage = new Stage();

            stage.initStyle(StageStyle.UNDECORATED);

            stage.setScene(scene);
            dialogController.start(stage);

            stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    System.out.println("new Refresh");
                }
            });
            stage.setAlwaysOnTop(true);
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @FXML
    void btnRemoveAction(ActionEvent event) {

    }

    @FXML
    void btnSaveAction(ActionEvent event) {

    }

    @FXML
    void btnViewDetailsAction(ActionEvent event) {

    }

    @FXML
    void txtDecsAction(ActionEvent event) {

    }

    @FXML
    void txtMajorCateFindAction(ActionEvent event) {

    }

    @FXML
    void txtPriceAction(ActionEvent event) {

    }

    @FXML
    void txtSubCateFindAction(ActionEvent event) {

    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        itemController = new com.example.resturent.controller.ItemController();
        subCateController = new SubCateController();
        majorCateId.setCellValueFactory(new PropertyValueFactory<>("majorCateId"));
        majorCateName.setCellValueFactory(new PropertyValueFactory<>("majorCateName"));
        majorCateDelete.setCellValueFactory(new PropertyValueFactory<>("majorCateDelete"));
        majorCateEdit.setCellValueFactory(new PropertyValueFactory<>("majorCateEdit"));
        subCateId.setCellValueFactory(new PropertyValueFactory<>("subCateId"));
        subCateName.setCellValueFactory(new PropertyValueFactory<>("subCateName"));
        subCateDelete.setCellValueFactory(new PropertyValueFactory<>("subCateDelete"));
        subCateEdit.setCellValueFactory(new PropertyValueFactory<>("subCateEdit"));

        categoryController = new CategoryController();
        stage = primaryStage;
        this.loadCategory();
        tblMajorCate.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    majorCateTableModel = (MajorCateTableModel) tblMajorCate.getItems().get(tblMajorCate.getSelectionModel().getSelectedIndex());

                    loadSubCateTable(majorCateTableModel.getMajorCateId());
                } catch (ArrayIndexOutOfBoundsException s) {

                } catch (NullPointerException d) {

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }

    private void loadCategory() {
        try {
            tableModels = FXCollections.observableArrayList();
            tableModels.clear();
            btnMajorDelete = new Button("Delete");
            btnMajorDelete.setStyle("-fx-background-radius: 60 60 60;-fx-background-color: #ff0600;-fx-text-fill: white");

            ArrayList<MajorCateDTO> all = categoryController.getAll();
            ArrayList<String> names = new ArrayList<>();
            for (MajorCateDTO m :
                    all) {
                CategoryStaticDB.majorCateDTOHashMap.put(m.getCate_name(), m);
                btnMajorEdit = new Button("Edie");
                btnMajorEdit.setStyle("-fx-background-radius: 60 60 60;-fx-background-color: #002fff;-fx-text-fill: white");
                btnMajorEdit.setOnAction(event -> {
                    try {
                        System.out.println("get Id: " + majorCateTableModel.getMajorCateId());
                    } catch (NullPointerException e) {
                        System.out.println("Select Row Before Delete");
                    }
                });
                names.add(m.getCate_name());
                tableModels.add(new MajorCateTableModel(m.getCate_id() + "", m.getCate_name(), btnMajorDelete, btnMajorEdit));
            }
            loadCategoryNames(names);
            tblMajorCate.setItems(tableModels);
            lock = true;
            chbMajorCate.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
                @Override
                public void changed(ObservableValue<? extends Number> observableValue, Number number, Number number2) {
                    MajorCate = chbMajorCate.getItems().get((Integer) number2);
                    loadSubCateByMajorCategory(chbMajorCate.getItems().get((Integer) number2));
                    if (!lock) {
                        loadCategory();
                    }
                }
            });

            chbSubCate.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
                @Override
                public void changed(ObservableValue<? extends Number> observableValue, Number number, Number number2) {

//                    loadSubCateByMajorCategory(chbSubCate.getItems().get((Integer) number2));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        btnMajorDelete.setOnAction(event -> {
            System.out.println("Custom Button Clicked");
            try {
                categoryController.remove(majorCateTableModel.getMajorCateId());
            } catch (NullPointerException w) {
                System.out.println("Select Before Delete");
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void loadCategoryNames(ArrayList<String> major) {

        ObservableList<String> m = FXCollections.observableArrayList();

        chbMajorCate.setValue("Select Major category");
        ArrayList<String> majorCateNames = major;

        for (String mn :
                majorCateNames) {
            m.add(mn);
        }
        this.chbMajorCate.setItems(m);
        this.chbMajorCate.getSelectionModel().selectFirst();
        loadSubCateByMajorCategory(chbMajorCate.getSelectionModel().getSelectedItem());

    }

    private void loadSubCateByMajorCategory(String id) {
        MajorCateDTO majorCateDTO = CategoryStaticDB.majorCateDTOHashMap.get(id);
        ArrayList<SubCateDTO> subCates = subCateController.getSubCategoryByMajorCategoryId(majorCateDTO.getCate_id() + "");
        ObservableList<String> c = FXCollections.observableArrayList();
        for (SubCateDTO s : subCates) {
            c.add(s.getSub_cate_name());
        }
        this.chbSubCate.setItems(c);
        this.chbSubCate.getSelectionModel().selectFirst();
    }

    @Override
    void refresh() {
        this.loadCategory();

    }

    public void chbMajorCateClick(MouseEvent mouseEvent) {
        try {
            if (chbMajorCate.getValue() != "Select Major category") {
                loadSubCateByMajorCategory(chbMajorCate.getSelectionModel().getSelectedItem());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadSubCateTable(String id) {

        ObservableList<SubCateTableModel> subCateTableModels = FXCollections.observableArrayList();
        ArrayList<SubCateDTO> subCategoryByMajorCategoryId = subCateController.getSubCategoryByMajorCategoryId(id);
        subCateTableModels.clear();
        for (SubCateDTO sub :
                subCategoryByMajorCategoryId) {
            Button btnDelete = new Button("Delete");
            btnDelete.setStyle("-fx-background-radius: 60 60 60;-fx-background-color: #ff0600;-fx-text-fill: white");
            Button btnEdit = new Button("Edie");
            btnDelete.setStyle("-fx-background-radius: 60 60 60;-fx-background-color: #002fff;-fx-text-fill: white");
            subCateTableModels.add(new SubCateTableModel(
                    sub.getSub_cate_id() + "",
                    sub.getSub_cate_name(),
                    btnDelete,
                    btnEdit
            ));
        }
        tblSubCate.setItems(subCateTableModels);
    }
}
