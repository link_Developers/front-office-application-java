package com.example.resturent.view.controller;

import com.example.resturent.util.PageValidator;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Line;
import javafx.stage.Stage;

import java.io.IOException;

public class PaymentController extends SuperController {
    private Stage stage;


    @FXML
    private BorderPane contanetPanel;

    @FXML
    private AnchorPane NavPanel;

    @FXML
    private Line CB;

    @FXML
    private Button btnBilling;

    @FXML
    private Button btnPayment;

    @FXML
    private Button btnConfirmAndPrint;

    @FXML
    private Line BP;

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.stage = primaryStage;
        loader("CollectionDetails");
    }

    @Override
    void refresh() {

    }

    void loader(String name) {
        Parent root = null;
        try {
//            root = FXMLLoader.load(getClass().getResource("/com/example/resturent/view/" + name + ".fxml"));
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/com/example/resturent/view/" + name + ".fxml"));
            root = fxmlLoader.load();
            SuperController controller = fxmlLoader.getController();
            controller.start(stage);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        contanetPanel.setCenter(root);
    }

    public void btnNextAction(ActionEvent actionEvent) {
        if (PageValidator.isCollectDetailsIsDone()) {
            btnBilling.getStyleClass().add("selected");
            Paint red;
            BP.setStroke(Paint.valueOf("red"));
            loader("JustPayment");

        }
    }

    public void btnResetAction(ActionEvent actionEvent) {
    }
}
