package com.example.resturent.view.controller;

import com.example.resturent.dto.RoomDTO;
import com.example.resturent.exception.CustomerNotFound;
import com.example.resturent.view.model.table.RoomTableModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.util.ArrayList;

public class RoomController extends SuperController {

    public TableView tblRoomTable1;
    @FXML
    private AnchorPane detailsPanel;

    @FXML
    private TextField txtRoomNumber;

    @FXML
    private TextField txtRoomType;

    @FXML
    private TextField txtPassword;

    @FXML
    private TextField txtConfirmPassword;

    @FXML
    private TextField txtBedCount;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnRemove;
    @FXML
    private CheckBox cbAllRoom;

    @FXML
    private CheckBox cbAvailableRoom;

    @FXML
    private CheckBox cbOcucupiedRoom;
    @FXML
    private TextField txtPrice;

    @FXML
    private Button btnBookNow;

    @FXML
    private TableColumn<?, ?> roomNumber;

    @FXML
    private TableColumn<?, ?> roomType;

    @FXML
    private TableColumn<?, ?> bedCount;

    @FXML
    private TableColumn<?, ?> price;

    @FXML
    private TableColumn<?, ?> gustId;

    @FXML
    private TableColumn<?, ?> status;

    @FXML
    private TextField txtFnd;

    @FXML
    private Button btnFind;

    @FXML
    private Label txtStatusMessage;
    private com.example.resturent.controller.RoomController roomController;
    private String FindRoomNumber = "";
    private RoomDTO findDto;

    private ObservableList<RoomTableModel> tableModels;

    @FXML
    void btnBookNowAction(ActionEvent event) {

    }

    @FXML
    void btnFindAction(ActionEvent event) {
        try {
            if (txtFnd.getText() != "") {
                findDto = roomController.find(txtFnd.getText());
                System.out.println(findDto);
                if (findDto == null) {
                    StatusMessage("Room Not Found ", CustomerController.MessageType.ERROR);
                }
                this.SetAllRoomDetailsTable(findDto);
            } else {
                this.SetAllRoomDetailsTable();

            }
        } catch (CustomerNotFound e) {
            StatusMessage("Room Not Found ", CustomerController.MessageType.ERROR);
        } catch (com.example.resturent.exception.CheckDetailsException
                e) {
//            StatusMessage("Recheck And try again ", CustomerController.MessageType.ERROR);
            this.SetAllRoomDetailsTable();
            StatusMessage("Table refresh ", CustomerController.MessageType.OK);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void btnRemoveAction(ActionEvent event) {
        try {
            String roomNo = findDto.getRoomNumber();

            boolean remove = roomController.remove(findDto.getRoomNumber());
            StatusMessage(roomNo + " is Removed", CustomerController.MessageType.OK);
        } catch (Exception e) {

        }
        this.refresh();
    }

    @FXML
    void btnSaveAction(ActionEvent event) {
        try {
            System.out.println(txtPassword.getText() + " == " + txtConfirmPassword.getText() + " = " + (txtPassword.getText().toString() == txtConfirmPassword.getText().toString()));

            String password;
            if (txtPassword.getText() != txtConfirmPassword.getText()) {
                System.out.println(txtPassword.getText() + " == " + txtConfirmPassword.getText() + " = " + (txtPassword.getText() == txtConfirmPassword.getText()));
                password = txtConfirmPassword.getText();

            } else {
                StatusMessage("Password not match", CustomerController.MessageType.ERROR);
                return;

            }
            if (FindRoomNumber == "") {
                roomController.add(new RoomDTO(
                        txtRoomNumber.getText(),
                        txtRoomType.getText(),
                        password,
                        Integer.parseInt(txtBedCount.getText()),
                        Double.parseDouble(txtPrice.getText()),
                        0,
                        0,
                        1,
                        "date"
                ));
                ClearAll();
            } else {
                RoomDTO temp = findDto;
                temp.setRoomType(txtRoomType.getText());
                temp.setPassword(txtConfirmPassword.getText());
                temp.setPrice(Double.parseDouble(txtPrice.getText()));
                temp.setBedCount(Integer.parseInt(txtBedCount.getText()));
                boolean update = roomController.update(temp);
                StatusMessage(temp.getRoomNumber() + " is Updated", CustomerController.MessageType.OK);
            }

        } catch (Exception e) {
        }
        this.refresh();
    }

    private void StatusMessage(String message, CustomerController.MessageType type) {
        txtStatusMessage.setText(message);
        switch (type) {
            case OK:
                txtStatusMessage.setStyle("-fx-text-fill: #00a900");
                break;
            case ERROR:
                txtStatusMessage.setStyle("-fx-text-fill: #ff0000");
                break;
            case WARING:
                txtStatusMessage.setStyle("-fx-text-fill: #a99000");
                break;

        }
    }

    @FXML
    void txtBedCountAction(ActionEvent event) {

    }

    @FXML
    void txtConfirmPasswordAction(ActionEvent event) {

    }

    @FXML
    void txtPasswordAction(ActionEvent event) {

    }

    @FXML
    void txtPriceAction(ActionEvent event) {

    }

    @FXML
    void txtRoomTypeAction(ActionEvent event) {

    }

    @Override
    void refresh() {
        this.SetAllRoomDetailsTable();
        this.ClearAll();
    }

    private void SetAllRoomDetailsTable(RoomDTO room) {
        try {
            System.out.println(room);
            tableModels = FXCollections.observableArrayList();
            tableModels.clear();
            String status = "";
            if (room.getUserId() == 0) {
                status = "Free";
            } else {
                status = "Booked";
            }
            ArrayList<RoomDTO> all = roomController.getAll();
            tableModels.add(
                    new RoomTableModel(
                            room.getRoomNumber(),
                            room.getRoomType(),
                            room.getBedCount() + "",
                            room.getPrice() + "",
                            room.getGuest() + "",
                            status
                    ));
            tblRoomTable1.setItems(tableModels);
        } catch (Exception e) {
        }
    }

    private void SetAllRoomDetailsTable() {
        try {
            cbAvailableRoom.setSelected(false);
            cbOcucupiedRoom.setSelected(false);
            cbAllRoom.setSelected(true);
            tableModels = FXCollections.observableArrayList();
            tableModels.clear();
            ArrayList<RoomDTO> all = roomController.getAll();
            for (RoomDTO room :
                    all) {
                String status = "";
                if (room.getUserId() == 0) {
                    status = "Free";
                } else {
                    status = "Booked";
                }
                tableModels.add(
                        new RoomTableModel(
                                room.getRoomNumber(),
                                room.getRoomType(),
                                room.getBedCount() + "",
                                room.getPrice() + "",
                                room.getGuest() + "",
                                status
                        ));
            }
            tblRoomTable1.setItems(tableModels);
        } catch (java.net.ConnectException e) {
            StatusMessage("No Connection ", CustomerController.MessageType.ERROR);
        } catch (Exception c) {

        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        roomController = new com.example.resturent.controller.RoomController();
        roomNumber.setCellValueFactory(new PropertyValueFactory<>("roomNumber"));
        roomType.setCellValueFactory(new PropertyValueFactory<>("roomType"));
        bedCount.setCellValueFactory(new PropertyValueFactory<>("bedCount"));
        price.setCellValueFactory(new PropertyValueFactory<>("price"));
        gustId.setCellValueFactory(new PropertyValueFactory<>("gustId"));
        status.setCellValueFactory(new PropertyValueFactory<>("status"));
        cbAllRoom.setSelected(true);
        this.SetAllRoomDetailsTable();
        this.findOnTableFillText();
    }

    private void findOnTableFillText() {
        tblRoomTable1.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    RoomTableModel tableModel = (RoomTableModel) tblRoomTable1.getItems().get(tblRoomTable1.getSelectionModel().getSelectedIndex());
                    FindRoomNumber = tableModel.getRoomNumber();
                    findDto = roomController.find(FindRoomNumber);
                    fillTextEdit(tableModel);
                } catch (ArrayIndexOutOfBoundsException s) {
                    StatusMessage("Please Select valid row ", CustomerController.MessageType.WARING);
                } catch (NullPointerException d) {

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void fillTextEdit(RoomTableModel roomTableModel) {
        this.FindRoomNumber = roomTableModel.getRoomNumber();
        this.StatusMessage(roomTableModel.getRoomNumber() + " is Selected", CustomerController.MessageType.OK);
        txtRoomNumber.setText(roomTableModel.getRoomNumber());
        txtRoomType.setText(roomTableModel.getRoomType());
        txtBedCount.setText(roomTableModel.getBedCount());
        txtPrice.setText(roomTableModel.getPrice());
        txtFnd.setText(roomTableModel.getRoomNumber());
    }

    private void ClearAll() {
        this.FindRoomNumber = "";
        cbAllRoom.setSelected(true);
        this.findDto = null;
        txtFnd.setText("");
        ObservableList<Node> children = detailsPanel.getChildren();
        for (Node node : detailsPanel.getChildren()) {
            if (node instanceof TextField) {
                final TextField textField = (TextField) node;
                textField.clear();
            }

        }
    }

    public void cbAllRoomAction(ActionEvent actionEvent) {
        cbAllRoom.setSelected(true);
        cbAvailableRoom.setSelected(false);
        cbOcucupiedRoom.setSelected(false);
        this.SetAllRoomDetailsTable();
    }

    public void cbAvailableRoomAction(ActionEvent actionEvent) {
        try {
            cbAllRoom.setSelected(false);
            cbAvailableRoom.setSelected(true);
            cbOcucupiedRoom.setSelected(false);
            this.SetAllRoomDetailsTable(roomController.getAllAvailable());
        } catch (Exception e) {
            cbAllRoom.setSelected(true);
            cbAvailableRoom.setSelected(false);
            cbOcucupiedRoom.setSelected(false);
            this.SetAllRoomDetailsTable();
            System.out.println("no data Available Rooms");
        }
    }

    public void cbOcucupiedRoomAction(ActionEvent actionEvent) {
        try {
            cbAllRoom.setSelected(false);
            cbAvailableRoom.setSelected(false);
            cbOcucupiedRoom.setSelected(true);
            this.SetAllRoomDetailsTable(roomController.getAllOccupiedRooms());
        } catch (Exception e) {
            cbAllRoom.setSelected(true);
            cbAvailableRoom.setSelected(false);
            cbOcucupiedRoom.setSelected(false);
            this.SetAllRoomDetailsTable();
            System.out.println("no data Occipied Rooms");
        }
    }

    private void SetAllRoomDetailsTable(ArrayList<RoomDTO> all) {
        try {
            tableModels = FXCollections.observableArrayList();
            tableModels.clear();
            for (RoomDTO room :
                    all) {
                String status = "";
                if (room.getUserId() == 0) {
                    status = "Free";
                } else {
                    status = "Booked";
                }
                tableModels.add(
                        new RoomTableModel(
                                room.getRoomNumber(),
                                room.getRoomType(),
                                room.getBedCount() + "",
                                room.getPrice() + "",
                                room.getGuest() + "",
                                status
                        ));
            }
            tblRoomTable1.setItems(tableModels);
        } catch (Exception e) {
        }
    }
}
