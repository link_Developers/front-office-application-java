package com.example.resturent.view.controller;

import com.example.resturent.dto.CustomerDTO;
import com.example.resturent.exception.CheckDetailsException;
import com.example.resturent.exception.CustomerNotFound;
import com.example.resturent.view.model.table.CustomerTableModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.util.ArrayList;

public class CustomerController extends SuperController {
    @FXML
    public Button btnRemove;
    @FXML
    public Button btnSave;
    @FXML
    public Button btnBookNow;
    @FXML
    public Button btnFind;
    @FXML
    public TextField txtFirstName;
    @FXML
    private TextField txtLastName;

    @FXML
    private TextField txtTp;

    @FXML
    private TextField txtEmail;

    @FXML
    private TextField txtId;
    @FXML
    private TextField txtFnd;
    @FXML
    private TableView customerTable;
    private ObservableList<CustomerTableModel> tableModels;
    @FXML
    private TableColumn<?, ?> gender;

    @FXML
    private TableColumn<?, ?> userId;

    @FXML
    private TableColumn<?, ?> firstName;

    @FXML
    private TableColumn<?, ?> lastName;

    @FXML
    private TableColumn<?, ?> tp;

    @FXML
    private TableColumn<?, ?> email;

    @FXML
    private TableColumn<?, ?> nic_passport;

    @FXML
    private TableColumn<?, ?> createdAt;
    @FXML
    private Label txtStatusMessage;
    private int findCustId;
    @FXML
    private Pane detailsPanel;
    @FXML
    private RadioButton rbtnMale;
    @FXML
    private RadioButton rbtnFemale;
    @FXML
    private RadioButton rbtnNic;
    @FXML
    private RadioButton rbtnPassport;
    private String itType;
    @FXML
    private CheckBox chByCustomerId;
    @FXML
    private CheckBox chByName;
    @FXML
    private CheckBox chByNicPassport;
    private com.example.resturent.controller.CustomerController customerController;

    @FXML
    void txtEmailAction(ActionEvent event) {

    }

    @FXML
    void txtIdActopm(ActionEvent event) {

    }

    @FXML
    void txtLastName(ActionEvent event) {

    }

    @FXML
    void txtTpAction(ActionEvent event) {

    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        customerController = new com.example.resturent.controller.CustomerController();
        userId.setCellValueFactory(new PropertyValueFactory<>("userId"));
        firstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        tp.setCellValueFactory(new PropertyValueFactory<>("tp"));
        email.setCellValueFactory(new PropertyValueFactory<>("email"));
        nic_passport.setCellValueFactory(new PropertyValueFactory<>("nic_passport"));
        gender.setCellValueFactory(new PropertyValueFactory<>("gender"));
        createdAt.setCellValueFactory(new PropertyValueFactory<>("createdAt"));
        this.setAllTableValue();
        findOnTableFillText();
        ToggleGroup idTypeGroup = new ToggleGroup();
        ToggleGroup genderGroup = new ToggleGroup();

        rbtnNic.setToggleGroup(idTypeGroup);
        rbtnPassport.setToggleGroup(idTypeGroup);
        rbtnMale.setToggleGroup(genderGroup);
        rbtnFemale.setToggleGroup(genderGroup);
        StatusMessage("Manage Customers Details", MessageType.OK);

    }

    @Override
    public void stop() throws Exception {
        super.stop();
        customerController = null;
        System.gc();
        System.out.println("System.gc()");

    }

    public void btnBookNowAction(ActionEvent actionEvent) {
//        setAllTableValue();
//        ArrayList<CustomerDTO> all = com.example.resturent.controller.CustomerController.getAll();
//        System.out.println("in view" + all.get(1));

        HomePageController instance = HomePageController.getInstance();
        instance.ChangeBackground(instance.getBtnBooking());

    }

    public void btnRemoveAction(ActionEvent actionEvent) {
        if (findCustId == 0) {
            StatusMessage("Please Select valid row ", MessageType.WARING);

            return;
        }
        try {
            boolean b = customerController.remove(findCustId + "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        StatusMessage(findCustId + " is Deleted", MessageType.OK);
        this.setAllTableValue();
        this.ClearAll();
    }

    private void ClearAll() {
        this.itType = "";
        this.findCustId = 0;
        ObservableList<Node> children = detailsPanel.getChildren();
        for (Node node : detailsPanel.getChildren()) {
            if (node instanceof TextField) {
                final TextField textField = (TextField) node;
                textField.clear();
                // all your logic
            }

        }
    }

    public void btnSaveAction(ActionEvent actionEvent) {
        try {

            if (findCustId == 0) {

                customerController.add(
                        new CustomerDTO(
                                1,
                                txtFirstName.getText(),
                                txtLastName.getText(),
                                txtTp.getText().toString(),
                                txtEmail.getText(),
                                txtId.getText(),
                                getIdType(),
                                getGender(),
                                "", false,
                                1
                        ));

                StatusMessage(txtFirstName.getText() + " is Added System", MessageType.OK);
                this.setAllTableValue();

            } else {
                if (findCustId == 0) {
                    StatusMessage("Please Select valid row ", MessageType.WARING);

                    return;
                }
                customerController.update(
                        new CustomerDTO(
                                findCustId,
                                txtFirstName.getText(),
                                txtLastName.getText(),
                                txtTp.getText().toString(),
                                txtEmail.getText(),
                                txtId.getText(),
                                getIdType(),
                                getGender(),
                                "", false,
                                1
                        ));
                StatusMessage(txtFirstName.getText() + " is Edited ", MessageType.OK);
                this.setAllTableValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void btnFindAction(ActionEvent actionEvent) {
        if (!txtFnd.getText().equals("")) {


            try {
                setFindValueInToTable(customerController.find(txtFnd.getText()));
            } catch (CheckDetailsException e) {
                StatusMessage("Check search details", MessageType.WARING);
                setAllTableValue();
            } catch (CustomerNotFound customerNotFound) {
                StatusMessage("Customer Not Found", MessageType.ERROR);
                setAllTableValue();
            } catch (Exception e) {

            }
        } else {
            setAllTableValue();
        }
    }

    public void setAllTableValue() {
        try {
            ClearAll();
            StatusMessage("Table refresh", MessageType.OK);
            tableModels = FXCollections.observableArrayList();
            tableModels.clear();
            ArrayList<CustomerDTO> all = customerController.getAll();
            for (CustomerDTO d :
                    all) {
                tableModels.add(new CustomerTableModel(
                        d.getCustomerId() + "",
                        d.getFirstName(),
                        d.getLastName(),
                        d.getTp(),
                        d.getEmail(),
                        d.getId(),
                        d.getGender(),
                        d.getCreatedAt()
                ));
            }
            System.out.println(tableModels);
            customerTable.setItems(tableModels);
            customerTable.getSortOrder().add(userId);
        } catch (Exception e) {

        }
    }

    private void setFindValueInToTable(CustomerDTO d) {
        tableModels.clear();
        try {
            tableModels.add(new CustomerTableModel(
                    d.getCustomerId() + "",
                    d.getFirstName(),
                    d.getLastName(),
                    d.getTp(),
                    d.getEmail(),
                    d.getId(),
                    d.getGender(),
                    d.getCreatedAt()
            ));
            itType = d.idType;
        } catch (NullPointerException e) {
            setAllTableValue();
            this.StatusMessage("Customer Not Found", MessageType.ERROR);

        }

    }

    private void StatusMessage(String message, MessageType type) {
        txtStatusMessage.setText(message);
        switch (type) {
            case OK:

                txtStatusMessage.setStyle("-fx-text-fill: #00a900");
                break;
            case ERROR:
                txtStatusMessage.setStyle("-fx-text-fill: #ff0000");
                break;
            case WARING:
                txtStatusMessage.setStyle("-fx-text-fill: #a99000");
                break;

        }
    }

    private void findOnTableFillText() {
        customerTable.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {

                    CustomerTableModel tableModel = (CustomerTableModel) customerTable.getItems().get(customerTable.getSelectionModel().getSelectedIndex());
                    findCustId = Integer.parseInt(tableModel.getUserId());
                    fillTextEdit(tableModel);
                } catch (ArrayIndexOutOfBoundsException s) {
                    StatusMessage("Please Select valid row ", MessageType.WARING);
                } catch (NullPointerException d) {

                }
            }
        });

    }

    private void fillTextEdit(CustomerTableModel customerTable) {
        this.StatusMessage(customerTable.getFirstName() + " is Selected", MessageType.OK);
        txtFirstName.setText(customerTable.getFirstName());
        txtLastName.setText(customerTable.getLastName());
        txtTp.setText(customerTable.getTp());
        txtEmail.setText(customerTable.getEmail());
        txtId.setText(customerTable.getNic_passport());
        if (customerTable.getGender().equals("Male")) {
            rbtnMale.setSelected(true);

        } else {
            rbtnFemale.setSelected(true);
        }
        if (itType.equals("NIC")) {
            rbtnNic.setSelected(true);
        } else {

            rbtnPassport.setSelected(true);

        }

    }

    public void rbtnMaleAction(ActionEvent actionEvent) {
    }

    public void rbtnFemale(ActionEvent actionEvent) {
    }

    public void rbtnNicActoin(ActionEvent actionEvent) {
    }

    public void rbtnPassportAction(ActionEvent actionEvent) {

    }

    private String getIdType() {
        if (rbtnNic.isSelected()) {
            return "NIC";
        } else {
            return "PASSPORT";
        }
    }

    private String getGender() {
        if (rbtnFemale.isSelected()) {
            return "Female";
        } else {
            return "Male";
        }


    }

    public void chByCustomerIdAction(ActionEvent actionEvent) {
    }

    public void chByNameAction(ActionEvent actionEvent) {
    }

    public void chByNicPassportAction(ActionEvent actionEvent) {
    }

    @Override
    void refresh() {
        this.setAllTableValue();
        findOnTableFillText();
    }

    public enum MessageType {
        ERROR, OK, WARING
    }

}
