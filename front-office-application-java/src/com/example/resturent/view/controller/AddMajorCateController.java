package com.example.resturent.view.controller;

import com.example.resturent.controller.CategoryController;
import com.example.resturent.dto.MajorCateDTO;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class AddMajorCateController extends SuperController {
    @FXML
    public TextField txtMCateName;
    private Stage stage;
    @FXML
    private AnchorPane MainPanel;
    private CategoryController categoryController;

    @Override
    void refresh() {

    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        categoryController = new CategoryController();
        this.stage = primaryStage;


    }

    public void systemExit(MouseEvent mouseEvent) {
        stage.close();

    }

    public void txtMCateName(ActionEvent actionEvent) {
    }

    public void btnSaveAction(ActionEvent actionEvent) {
        try {
            categoryController.add(new MajorCateDTO(0, txtMCateName.getText(), "", 1));
        } catch (Exception e) {
            stage.close();
        }
        stage.close();
    }

    public void btnCloseMouseEnterd(MouseEvent mouseEvent) {
    }

    public void btnCloseMouseExit(MouseEvent mouseEvent) {
    }

}
