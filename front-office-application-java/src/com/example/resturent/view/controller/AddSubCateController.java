package com.example.resturent.view.controller;

import com.example.resturent.controller.CategoryController;
import com.example.resturent.controller.SubCateController;
import com.example.resturent.dto.MajorCateDTO;
import com.example.resturent.dto.SubCateDTO;
import com.example.resturent.util.CategoryStaticDB;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.util.ArrayList;

public class AddSubCateController extends SuperController {
    @FXML
    public TextField txtMCateName;
    private Stage stage;
    @FXML
    private ChoiceBox<String> chCate;
    private SubCateController subCateController;

    @Override
    void refresh() {

    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.stage = primaryStage;
        subCateController = new SubCateController();
        ObservableList<String> objects = FXCollections.observableArrayList();
        ArrayList<MajorCateDTO> majorCateNames = new CategoryController().getAll();
        for (MajorCateDTO m :
                majorCateNames) {
            CategoryStaticDB.majorCateDTOHashMap.put(m.getCate_name(), m);
            objects.add(m.getCate_name())
            ;
        }
        System.out.println("HashMap" + CategoryStaticDB.majorCateDTOHashMap);
        chCate.setItems(objects);

    }

    public void btnCloseMouseEnterd(MouseEvent mouseEvent) {
        this.stage.close();
    }


    public void btnSaveAction(ActionEvent actionEvent) {
        try {

            System.out.println("Selected Model : " + chCate.getValue().toString());
            MajorCateDTO majorCateDTO = CategoryStaticDB.majorCateDTOHashMap.get(chCate.getValue());

            subCateController.add(new SubCateDTO(
                    0,
                    txtMCateName.getText(),
                    majorCateDTO.getCate_id(),
                    "", 1
            ));
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.stage.close();
    }

    public void btnCloseMouseExit(MouseEvent mouseEvent) {
    }

    public void systemExit(MouseEvent mouseEvent) {
        this.stage.close();
    }

    public void txtMCateName(ActionEvent actionEvent) {
    }
}
