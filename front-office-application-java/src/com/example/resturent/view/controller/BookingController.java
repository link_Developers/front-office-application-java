package com.example.resturent.view.controller;

import com.example.resturent.controller.BookingManageController;
import com.example.resturent.controller.CustomerController;
import com.example.resturent.controller.RoomController;
import com.example.resturent.dto.BookingDTO;
import com.example.resturent.dto.CustomerDTO;
import com.example.resturent.dto.RoomDTO;
import com.example.resturent.view.model.chBoxModel.CustomerModel;
import com.example.resturent.view.model.table.RoomTableModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.util.ArrayList;

public class BookingController extends SuperController {

    @FXML
    public TableColumn<?, ?> gustId;
    @FXML
    public TableColumn<?, ?> status;
    @FXML
    public TableColumn<?, ?> gustId1;
    @FXML
    public TableColumn<?, ?> status1;
    @FXML
    private AnchorPane detailsPanel;
    @FXML
    private ChoiceBox<CustomerModel> chbMajorCate;
    @FXML
    private Button btnAddMejorCate;
    @FXML
    private Button btnSave;
    @FXML
    private Button btnRemove;
    @FXML
    private Button btnViewDetails;
    @FXML
    private Button btnTblAdd;
    @FXML
    private Button btnTblRemove;
    @FXML
    private Button btnSearch;
    @FXML
    private TextField txtSerach;
    private ArrayList<CustomerDTO> customerDTOArrayList;
    private BookingManageController bookingManageController;
    private CustomerController customerController;
    private RoomController roomController;
    @FXML
    private TableView<RoomTableModel> tblSelectedRoom;
    @FXML
    private TableColumn<?, ?> roomNumber;
    @FXML
    private TableColumn<?, ?> bedCount;
    @FXML
    private TableColumn<?, ?> roomType;
    @FXML
    private TableColumn<?, ?> price;
    @FXML
    private TableView<RoomTableModel> tblAvailableRoom;
    @FXML
    private TableColumn<?, ?> roomNumber1;
    @FXML
    private TableColumn<?, ?> bedCount1;
    @FXML
    private TableColumn<?, ?> roomType1;
    @FXML
    private TableColumn<?, ?> price1;
    private ObservableList<RoomTableModel> availableRoomList;
    private ObservableList<RoomTableModel> SelectedRoomModels;
    private ArrayList<RoomDTO> roomDTOSForOrder = new ArrayList<>();

    @FXML
    void btnAddMejorCateAction(ActionEvent event) {
        HomePageController instance = HomePageController.getInstance();

        instance.ChangeBackground(instance.getBtnCustomer());
//        instance.ChangePanel("Customer");
    }

    @FXML
    void btnRemoveAction(ActionEvent event) {

    }

    @FXML
    void btnSaveAction(ActionEvent event) {
        this.SaveButtonAction();
    }

    @FXML
    void btnTblAddAction(ActionEvent event) {
        addButtonAction();
    }

    @FXML
    void btnTblRemoveAction(ActionEvent event) {
        this.removeButtonAction();
    }

    @FXML
    void btnViewDetailsAction(ActionEvent event) {

    }

    @FXML
    void chbMajorCateClick(MouseEvent event) {

    }

    @FXML
    void txtSerachAction(ActionEvent event) {

    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.bookingManageController = new BookingManageController();
        this.roomController = new RoomController();
        this.customerController = new CustomerController();

        //Selected room
        roomNumber.setCellValueFactory(new PropertyValueFactory<>("roomNumber"));
        roomType.setCellValueFactory(new PropertyValueFactory<>("roomType"));
        bedCount.setCellValueFactory(new PropertyValueFactory<>("bedCount"));
        price.setCellValueFactory(new PropertyValueFactory<>("price"));
        gustId.setCellValueFactory(new PropertyValueFactory<>("gustId"));
        status.setCellValueFactory(new PropertyValueFactory<>("status"));

        // Available rooms
        roomNumber1.setCellValueFactory(new PropertyValueFactory<>("roomNumber"));
        roomType1.setCellValueFactory(new PropertyValueFactory<>("roomType"));
        bedCount1.setCellValueFactory(new PropertyValueFactory<>("bedCount"));
        price1.setCellValueFactory(new PropertyValueFactory<>("price"));
        gustId1.setCellValueFactory(new PropertyValueFactory<>("gustId"));
        status1.setCellValueFactory(new PropertyValueFactory<>("status"));
        SelectedRoomModels = FXCollections.observableArrayList();

        this.loadCustomerList();
        this.loadAvailableRooms();

    }

    @Override
    void refresh() {
        this.loadCustomerList();
        loadAvailableRooms();
    }

    private void loadCustomerList() {
        try {
            ObservableList<CustomerModel> listName = FXCollections.observableArrayList();
            this.customerDTOArrayList = this.customerController.getAll();
            for (CustomerDTO cust :
                    this.customerDTOArrayList) {
                listName.add(new CustomerModel(cust.getFirstName() + " " + cust.getLastName(), cust));
            }
            chbMajorCate.setItems(listName);
            chbMajorCate.getSelectionModel().selectFirst();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadAvailableRooms() {
        try {
            availableRoomList = FXCollections.observableArrayList();
            ArrayList<RoomDTO> allAvailable = roomController.getAllAvailable();
            for (RoomDTO r :
                    allAvailable) {
                availableRoomList.add(new RoomTableModel(
                        r.getRoomNumber(), r.getRoomType(), r.getBedCount() + "", r.getPrice() + "", 0 + "", 1 + ""
                ));
            }
            tblAvailableRoom.setItems(availableRoomList);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void addButtonAction() {
        RoomTableModel tableModel = (RoomTableModel) tblAvailableRoom.getItems().get(tblAvailableRoom.getSelectionModel().getSelectedIndex());

        SelectedRoomModels.add(tableModel);
        availableRoomList.remove(tableModel);
        tblAvailableRoom.setItems(availableRoomList);
        tblSelectedRoom.setItems(SelectedRoomModels);
    }

    private void removeButtonAction() {
        System.out.println("removeButton action");
        RoomTableModel tableModel = (RoomTableModel) tblSelectedRoom.getItems().get(tblSelectedRoom.getSelectionModel().getSelectedIndex());
//roomDTOSForOrder.remove(tableModel.getRoomNumber());
        SelectedRoomModels.remove(tableModel);
        availableRoomList.add(tableModel);
        tblAvailableRoom.setItems(availableRoomList);
        tblSelectedRoom.setItems(SelectedRoomModels);
    }

    private void SaveButtonAction() {
        ObservableList<RoomTableModel> items = tblSelectedRoom.getItems();
        for (RoomTableModel rtm :
                items) {

            roomDTOSForOrder.add(new RoomDTO(
                    rtm.getRoomNumber(),
                    rtm.getRoomType(),
                    "",
                    Integer.parseInt(rtm.getBedCount()),
                    Double.parseDouble(rtm.getPrice()),
                    Integer.parseInt(rtm.getGustId()),
                    Integer.parseInt(rtm.getGustId()),
                    1,
                    ""
            ));

        }
        try {
            SingleSelectionModel<CustomerModel> selectionModel = chbMajorCate.getSelectionModel();
            bookingManageController.add(
                    new BookingDTO(
                            selectionModel.getSelectedItem().getCustomerDTO().getCustomerId(),
                            "Front Office",
                            roomDTOSForOrder)
            );
//            System.out.println("Selected Rooms : " + roomDTOSForOrder.size());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
